package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_codigo.view.opc
import kotlinx.android.synthetic.main.item_historico_erros.view.*


class AdapterListCompleted(val codigos: MutableList<Codigo>, val context: Context) :
    RecyclerView.Adapter<ViewHolderConclu>() {

    var items = ArrayList<Codigo>(codigos)
    override fun onBindViewHolder(p0: ViewHolderConclu, p1: Int) {
        if (items.get(p1).rota_bip != null && items.get(p1).rota_bip != "") {
            p0.codigo?.text = items.get(p1).numero
            p0.rota?.text = items.get(p1).rota_bip
            if (items.get(p1).descricao != "null") {
                p0.descricao?.text = items.get(p1).descricao
            } else {
                p0.descricao?.text = ""
            }
            p0.cpf?.text = items.get(p1).cpfbipador
            p0.data?.text = items.get(p1).databipar
        } else {
            p0.ct_bip?.visibility = View.GONE
            p0.ct_erro?.visibility = View.VISIBLE

        }
        if (items.get(p1).rota_veri != null && items.get(p1).rota_veri != "") {
            p0.codigo?.text = items.get(p1).numero
            p0.rota_veri?.text = items.get(p1).rota_veri
            if (items.get(p1).descricaoveri != "null") {
                p0.descricao_veri?.text = items.get(p1).descricaoveri
            } else {
                p0.descricao_veri?.text = ""
            }
            p0.cpf_veri?.text = items.get(p1).cpfveri
            p0.data_veri?.text = items.get(p1).dataveri
        } else {
            p0.ct_veri?.visibility = View.GONE
            p0.ct_erro_veri?.visibility = View.VISIBLE
        }
        p0.opc?.visibility = View.GONE
    }

    fun filter(query: String) {
        items.clear()
        if (!query.isEmpty()) {
            for (x in codigos!!) {
                Log.e("codigostext", query + "teste" )
                if (x.numero.contains(query)
                ) {
                    Log.e("x", items.toString())
                    items.add(x)
                }
            }
        } else {
            items.addAll(codigos!!)
        }
        Log.e("x", items.toString())
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderConclu {
        return ViewHolderConclu(
            LayoutInflater.from(context).inflate(
                R.layout.item_historico_erros,
                p0,
                false
            )
        )
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}


class ViewHolderConclu(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val codigo = itemView.val_codigo
    val rota = itemView.rota
    val rota_veri = itemView.rota_veri
    val descricao = itemView.descricao
    val descricao_veri = itemView.descricao_veri
    val cpf = itemView.cpf
    val cpf_veri = itemView.cpf_veri
    val data = itemView.data
    val data_veri = itemView.data_veri
    val ct_bip = itemView.conteudo_bip
    val ct_veri = itemView.conteudo_verif
    val ct_erro = itemView.bip_pendente
    val ct_erro_veri = itemView.verif_pendente
    val opc = itemView.opc
}