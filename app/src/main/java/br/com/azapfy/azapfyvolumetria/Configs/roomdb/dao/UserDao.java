package br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao;

import android.arch.persistence.room.*;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("DELETE  FROM  user  WHERE cnpj = :x ")
    void deleteruser(String x);
    @Query("SELECT * FROM user WHERE auth = :auth LIMIT 1")
    User findAuth(boolean auth);

    @Query("SELECT * FROM user WHERE cpf = :cpf LIMIT 1")
    User findByCpf(String cpf);

    @Query("SELECT base FROM user WHERE cpf = :cpf")
    String findByBase(String cpf);
    @Query("DELETE FROM  User")
    void drop();

    @Query("DELETE FROM Codigo")
    void dropcodigo();

    @Query("DELETE FROM Romaneio")
    void dropromaneio();

    @Query("DELETE FROM Rota")
    void dropvolume();

    @Query("DELETE FROM Base")
    void dropbase();

    @Query("DELETE FROM SubRota")
    void dropsubrota();
    @Insert
    void insert(User user);

    @Insert
    void insertAll(List<User> users);

    @Update
    void update(User user);

    @Delete
    void delete(User user);

}