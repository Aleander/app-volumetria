package br.com.azapfy.azapfyvolumetria.Gestor.Adapter
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_historico_erros.view.*

class AdapterListFilter(val codigos: Codigo, val context: Context) :
    RecyclerView.Adapter<ViewHolderFilter>() {

    lateinit var pass: User
    lateinit var passs: Codigo
    var items = codigos

    init {
        passs = Codigo()
        pass = User()
    }

    override fun onBindViewHolder(p0: ViewHolderFilter, p1: Int) {
        Log.e("items", items.numero + "sdasa")
        if (items.rota_bip != null && items.rota_bip != "") {
            p0.codigo?.text = items.numero
            p0.rota?.text = items.rota_bip
            if (items.descricao != "null") {
                p0.descricao?.text = items.descricao
            } else {
                p0.descricao?.text = ""
            }
            p0.cpf?.text = items.cpfbipador
            p0.data?.text = items.databipar
        } else {
            p0.ct_bip?.visibility = View.GONE
            p0.ct_erro?.visibility = View.VISIBLE
        }

        if (items.rota_veri != null && items.rota_veri != "") {
            p0.codigo?.text = items.numero
            p0.rota_veri?.text = items.rota_veri
            if (items.descricaoveri != "null") {
                p0.descricao_veri?.text = items.descricaoveri
            } else {
                p0.descricao_veri?.text = ""
            }
            p0.cpf_veri?.text = items.cpfveri
            p0.data_veri?.text = items.dataveri
        } else {
            p0.ct_veri?.visibility = View.GONE
            p0.ct_erro_veri?.visibility = View.VISIBLE
        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderFilter {
        return ViewHolderFilter(
            LayoutInflater.from(context).inflate(
                R.layout.item_historico_erros,
                p0,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return 1
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}


class ViewHolderFilter(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val codigo = itemView.val_codigo
    val rota = itemView.rota
    val rota_veri = itemView.rota_veri
    val descricao = itemView.descricao
    val descricao_veri = itemView.descricao_veri
    val cpf = itemView.cpf
    val cpf_veri = itemView.cpf_veri
    val data = itemView.data
    val data_veri = itemView.data_veri
    val ct_bip = itemView.conteudo_bip
    val ct_veri = itemView.conteudo_verif
    val ct_erro = itemView.bip_pendente
    val ct_erro_veri = itemView.verif_pendente
    val opc = itemView.opc
}

