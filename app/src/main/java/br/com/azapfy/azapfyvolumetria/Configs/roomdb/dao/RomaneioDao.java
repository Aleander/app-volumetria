package br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao;

import android.arch.persistence.room.*;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Romaneio;

import java.util.List;


@Dao
public interface RomaneioDao {
    @Query("SELECT * FROM Romaneio WHERE situacao = :situacao ")
    List<Romaneio> getRomaneioSituacao(String situacao);


    @Query("SELECT *  FROM Romaneio")
    List<Romaneio> findallromaneio();
    @Insert
    void insert(Romaneio Romaneio);


    @Update
    void update(Romaneio Romaneio);

    @Delete
    void delete(Romaneio Romaneio);

    @Query("SELECT  _id FROM Romaneio  WHERE _id = :id")
    String findveriRomaneioId(String id);
}
