package br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index(value = {"id" , "base"}, unique = true)})
public class Base {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "base")
    public String base;
    @ColumnInfo(name = "cpnj")
    public String cpnj ;

}