
package br.com.azapfy.azapfyvolumetria.Configs.url

import android.content.Context
import java.net.URL

object ServicesUrl {
    const val BASIC = 0
    const val VALIDATE = 1
    const val ROMANEIOCREATE = 2
    const val PESQUISARCODIGO = 3
    const val CONFIRMARGESTOR = 4
    const val ENVIARHISTORICO = 5
    const val ROTACREATE = 6
    const val SUBROTA = 7
    const val CRIARUSER = 8
    const val SUBROTARETURN = 9
    const val VALIDARCODIGO = 10
    const val RETORNACODIGOS = 11
    const val RETORNAROTAS = 12
    const val APROVARCODIGO = 13
    const val ENVIARCODIGOROMANEIO = 14
    fun getServiceURL(ctx: Context?, code: Int): String {
        if (ctx == null) {
            return ""
        }
        val base = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_LOCAL
        val pesquisar = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.PESQUISARCODIGO
        val romaneio = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.ENVIARROMANEIO
        val gestor = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.CONFIRMARGESTOR
        val enviarhistorico = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.ENVIARHISTORICO
        val criarROTA = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.ROTACREATE
        val criarSubRota = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.SUBROTA
        val criarUser = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.CRIARUSER
        val subretorna = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.RETORNASUBROTA
        val verificar = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.VERIFICARCODIGO
        val retornaCodigos = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.RETORNACODIGOS
        val retornaRotas = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.RETORNAROTAS
        val aprovarCodigo = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.APROVARCODIGO
        val enviarromacodigo = br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.ENVIARCODIGOROMA
        return when (code) {
            BASIC -> base
            CONFIRMARGESTOR -> gestor
            ROMANEIOCREATE -> romaneio
            PESQUISARCODIGO -> pesquisar
            VALIDATE -> base.replace("android/", "login/")
            ENVIARHISTORICO -> enviarhistorico
            ROTACREATE -> criarROTA
            SUBROTA -> criarSubRota
            CRIARUSER -> criarUser
            SUBROTARETURN -> subretorna
            VALIDARCODIGO -> verificar
            RETORNACODIGOS -> retornaCodigos
            RETORNAROTAS -> retornaRotas
            APROVARCODIGO -> aprovarCodigo
            ENVIARCODIGOROMANEIO -> enviarromacodigo
            else -> base
        }
    }

    fun getServiceCode(url: URL): Int {
        return when {
            url.toString().contains("login/") -> VALIDATE
            else -> BASIC
        }
    }
}
