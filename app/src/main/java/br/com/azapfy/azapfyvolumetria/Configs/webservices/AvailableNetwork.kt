package br.com.azapfy.azapfyvolumetria.Configs.webservices

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Created by Lucas on 23/03/2018.
 */

object AvailableNetwork {

    internal var result = "none"

    fun isNetworkAvaliable(ctx: Context): String {
        var status = String()
        try {
            val connectivityManager = ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager


            status = if (connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED) {
                //return true;
                "ConnectivityManager.TYPE_MOBILE"
            } else if (connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                            .state == NetworkInfo.State.CONNECTED) {
                "ConnectivityManager.TYPE_WIFI"

            } else {
                "none"
            }
            return status
        } catch (e: Exception) {
            return "none"
        }

    }

    fun isNetworkAvaliable(ctx: Context, src: Int): String {
        val status: String
        try {
            val connectivityManager = ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager


            if (connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED) {
                status = "ConnectivityManager.TYPE_MOBILE"
            } else if (connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                            .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                            .state == NetworkInfo.State.CONNECTED) {
                status = "ConnectivityManager.TYPE_WIFI"
            } else {
                status = "none"
            }
            return status
        } catch (e: Exception) {
            return "none"
        }

    }

}
