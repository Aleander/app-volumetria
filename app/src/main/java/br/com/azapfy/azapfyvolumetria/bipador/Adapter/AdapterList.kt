package br.com.azapfy.azapfyvolumetria.bipador.Adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.PopupMenu
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Activity.BarcodeReaderFragment
import br.com.azapfy.azapfyvolumetria.bipador.fragment.FragmentListNew
import kotlinx.android.synthetic.main.item_codigo.view.*


class AdapterList(val items: MutableList<Codigo>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    lateinit var mAdapter: FragmentListNew
    lateinit var pass: User

    init {
        pass = User()
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
            if (items.get(p1).rota_veri == null) {

            } else {
                p0.codigo?.text = items.get(p1).numero
                p0.descri.visibility = View.GONE
                p0.obs.visibility = View.GONE
                p0.opc.visibility = View.GONE

                if (items.get(p1).descricaoveri != "null" && items.get(p1).descricaoveri != ""
                    && items.get(p1).descricaoveri != null
                ) {
                    p0.descri?.text = items.get(p1).descricaoveri
                    p0.descri.visibility = View.VISIBLE
                    p0.obs.visibility = View.VISIBLE
                }
            }
        } else {
            if (items.get(p1).numero == null) {

                p0.complet.removeAllViews()
                p0.itemView.setVisibility(View.GONE)

            } else {
                p0.codigo?.text = items.get(p1).numero
                p0.descri.visibility = View.GONE
                p0.obs.visibility = View.GONE
                Log.e("teste", items.get(p1).descricao.toString())
                if (items.get(p1).descricao.toString() != "null" && items.get(p1).descricao.toString() != "") {
                    p0.descri?.text = items.get(p1).descricao.toString()
                    p0.descri.visibility = View.VISIBLE
                    p0.obs.visibility = View.VISIBLE
                }
            }
        }

        p0.opc.setOnClickListener {
            val v = p0.opc
            val location = IntArray(2)
            v.getLocationOnScreen(location)
            //creating a popup menu
            val popup = PopupMenu(context, v)
            //inflating menu from xml resource
            if (p0.descri.text != "null" && p0.descri.text != "") {
                popup.inflate(br.com.azapfy.azapfyvolumetria.R.menu.menu_comentario)
            } else {
                popup.inflate(br.com.azapfy.azapfyvolumetria.R.menu.menu_main)
            }
            //adding click listener
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    when (item.itemId) {
                        br.com.azapfy.azapfyvolumetria.R.id.adicicome -> {
                            val input = EditText(context)
                            val builder = AlertDialog.Builder(context)
                            builder.setTitle("Adicionar Observação")
                            input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_DATETIME_VARIATION_NORMAL
                            builder.setView(input)
                            builder.setPositiveButton("OK",
                                DialogInterface.OnClickListener { dialog, which ->
                                    Thread {
                                        val novaCodigo = Codigo()
                                        novaCodigo.numero = items.get(p1).numero
                                        novaCodigo.descricao = input.text.toString()
                                    }.start()
                                    p0.descri.text = input.text.toString()
                                    p0.descri.visibility = View.VISIBLE
                                    p0.obs.visibility = View.VISIBLE
                                }
                            )
                            builder.setNegativeButton("Cancel",
                                DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
                            builder.show()
                        }
                        br.com.azapfy.azapfyvolumetria.R.id.excluiroitem -> {
                            Thread {
                                BarcodeReaderFragment().listar.remove(items.get(p1).numero)
                                Log.e("/,", BarcodeReaderFragment().listar.toString())
                                BarcodeReaderFragment().verifi = false
                                AppDatabase.use(context).codigoDao()
                                    .detelecodigo(Values.getPassId(), items.get(p1).numero)
                            }.start()
                            p0.complet.visibility = View.GONE
                            items.removeAt(p0.adapterPosition);
                            notifyItemRemoved(p0.adapterPosition);
                            notifyItemRangeChanged(p0.adapterPosition, items.size);
                        }
                        br.com.azapfy.azapfyvolumetria.R.id.excluircome -> {

                            Thread {
                                val novaCodigo = Codigo()
                                novaCodigo.numero = items.get(p1).numero
                                novaCodigo.descricao = ""
                            }.start()
                            p0.descri.text = ""
                            p0.descri.visibility = View.GONE
                            p0.obs.visibility = View.GONE
                        }
                        br.com.azapfy.azapfyvolumetria.R.id.editar -> {
                            val input = EditText(context)
                            val builder = AlertDialog.Builder(context)
                            builder.setTitle("Adicionar Observação")
                            input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_DATETIME_VARIATION_NORMAL
                            builder.setView(input)
                            builder.setPositiveButton("OK",
                                DialogInterface.OnClickListener { dialog, which ->
                                    Thread {
                                        val novaCodigo = Codigo()
                                        novaCodigo.numero = items.get(p1).numero
                                        novaCodigo.descricao = ""
                                    }.start()

                                    p0.descri.text = input.text.toString()
                                }
                            )
                            builder.setNegativeButton("Cancel",
                                DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
                            builder.show()
                        }
                    }//handle menu1 click
                    //handle menu2 click
                    //handle menu3 click
                    return false
                }
            })
            //displaying the popup
            popup.show()
        }
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_codigo,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }
}


class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,

    View.OnLongClickListener {

    override fun onLongClick(v: View?): Boolean {

        return false
    }

    val codigo = itemView.numeroc
    val descri = itemView.descri
    val obs = itemView.obs
    val opc = itemView.opc
    val complet = itemView.complet

    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    override fun onClick(view: View) {

    }

}

