package br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao;

import android.arch.persistence.room.*;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.SubRota;

import java.util.List;


@Dao
public interface SubRotaDao {

    @Insert
    void insert(SubRota SubRota);

    @Query("SELECT *  FROM SubRota")
    List<SubRota> findallrotas();

    @Query("SELECT  * FROM SubRota  WHERE rota = :rota ")
    List<SubRota> findrotas(String rota);

    @Query("SELECT  id_rota FROM SubRota  WHERE id_rota = :numero ")
    String findveri(String numero);

    @Query("SELECT count(*) FROM SubRota WHERE rota = :rota")
    String count(String rota);

    @Update
    void update(SubRota SubRota);

    @Delete
    void delete(SubRota SubRota);
}
