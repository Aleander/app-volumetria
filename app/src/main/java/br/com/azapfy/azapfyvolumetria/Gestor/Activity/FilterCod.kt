package br.com.azapfy.azapfyvolumetria.Gestor.Activity


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.Configs.utils.SimpleDialog
import br.com.azapfy.azapfyvolumetria.Configs.webservices.AvailableNetwork
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListFilter
import br.com.azapfy.azapfyvolumetria.R
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_filter.*
import org.json.JSONObject


class FilterCod : AppCompatActivity() {
    private val disposable = CompositeDisposable()
    var veri = 0
    lateinit var mAdapter: AdapterListFilter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        imageButton!!.setOnClickListener {
            if (AvailableNetwork.isNetworkAvaliable(this, 0) == "none") {
                SimpleDialog.show(
                    this, "Oops! Não há conexão com a internet. " +
                            "Verifique" +
                            " sua conexão e tente novamente."
                )
                return@setOnClickListener
            } else {
                val json = JSONObject()
                var text = searchInput.text.toString()
                json.put("codigo", text)
                json.put("cnpj_empresa", "${Values.getCnpj()}")
                val connection = Connection(
                    ServicesUrl.getServiceURL(application, ServicesUrl.PESQUISARCODIGO), json,
                    this
                )
                Log.e("Criado", json.toString())
                //enviar coisas e recuperar o json

                recyclerView.layoutManager = LinearLayoutManager(applicationContext)
                Thread{
                var x  =  HttpPost().postPesquisa(connection, this)
                    runOnUiThread {
                        Log.e("xxxx", x.numero.toString() )
                        mAdapter = AdapterListFilter(x, this)
                    }
                }.start()

                recyclerView.adapter = mAdapter
                mAdapter = AdapterListFilter(Codigo(), this)
            }
        }
    }


    fun list() {
        veri = 1
        Log.e("list", veri.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}