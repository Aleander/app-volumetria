package br.com.azapfy.azapfyvolumetria.Configs.roomdb.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao.*;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.*;


@Database(entities = {User.class, Rota.class, Codigo.class, Romaneio.class, Base.class, SubRota.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    /**
     * Migrations
     * About {@link #MIGRATION1_2} Just a sample of how migrations works
     */


    public static final Migration MIGRATION1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //database.execSQL("ALTER TABLE user ADD COLUMN configs TEXT");
        }
    };
    private static final String DATABASE_NAME = "application-database-azapfyvolumetria";
    private static AppDatabase INSTANCE;
    /**
     * Singleton
     *
     * @use: returns a singleton database
     */
    public static AppDatabase use(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,
                            DATABASE_NAME)
                            .build();
        }
        return INSTANCE;
    }

    /**
     * DAO
     *
     * @UserDao: corresponds to @User table commands
     * @NotaDao: corresponds to @Rota table commands
     * @RomaneioDao: corresponds to @GroupRomaneio data manager
     * @RemetenteDao: corresponds to @GroupRemetente data manager
     */
    public abstract UserDao userDao();

    public abstract RotaDao volumeDao();

    public abstract SubRotaDao subRotaDao();

    public abstract CodigoDao codigoDao();

    public abstract RomaneioDao romaneioDao();

    public abstract BaseDao baseDao();
}