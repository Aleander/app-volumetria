package br.com.azapfy.azapfyvolumetria.bipador.Adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Activity.Camera
import br.com.azapfy.azapfyvolumetria.bipador.Activity.ListaRotasConcluidas
import kotlinx.android.synthetic.main.item_rotaspermanentes.view.*
import java.util.*


class AdapterListRotaPermanentes(
    val items: MutableList<Rota>,
    val context: Context?
) : RecyclerView.Adapter<ViewHoldeRotaPermanente>() {
    var rotas = ArrayList<Rota>(items)
    lateinit var pass: User

    init {
        pass = User()
    }

    override fun onBindViewHolder(p0: ViewHoldeRotaPermanente, p1: Int) {
        p0.rota?.text = rotas.get(p1).rota
        p0.id = rotas.get(p1)._id
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldeRotaPermanente {
        return ViewHoldeRotaPermanente(
            LayoutInflater.from(context).inflate(
                R.layout.item_rotaspermanentes,
                p0,
                false
            )
        )
    }


    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return rotas.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
    fun filter(query: String) {
        rotas.clear()
        if (!query.isEmpty()) {
            for (rota in items!!) {
                if ((rota).rota!!.toUpperCase().contains(query.toUpperCase())) {
                    Log.e("x", rotas.toString())
                    rotas.add(rota)
                }
            }
        } else {
            rotas.addAll(items!!)
        }
        Log.e("x", rotas.toString())
        notifyDataSetChanged()
    }
}


class ViewHoldeRotaPermanente(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {

    override fun onLongClick(v: View?): Boolean {
        return false
    }

    var id = ""
    val rota = itemView.rotapermanente
    val add = itemView.add
    val rotaperma = itemView.rotapermanente

    init {

        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
        add.setOnClickListener {
            lateinit var pass: User
            lateinit var passC: Codigo
            VolleyGet.AtualizarCodigos(Activity(), itemView.getContext(), Activity())
            Thread {

                pass = User()
                passC = Codigo()
                passC.setPass(1)
                Values.setPassTipo("rota")
                Values.setPassRota(rota.text.toString())
                Values.setPassId(id)
                Log.e("dsad", id + "ssadas")
                val intent = Intent(itemView.getContext(), Camera::class.java)
                itemView.getContext().startActivity(intent)

            }.start()

        }
        rotaperma.setOnClickListener {
          val intent = Intent(itemView.getContext(), ListaRotasConcluidas::class.java)
            intent.putExtra( "rota",rota.text.toString() )
            itemView.getContext().startActivity(intent)
            /*  val data = Date()
              val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
              val cal = Calendar.getInstance()
              cal.time = data
              val data_atual = cal.time
              val data_completa = dateFormat_hora.format(data_atual)
              Log.e("data_completa", data_completa)
              val json = JSONObject()
              json.put("rota", rota.toString())
              json.put("cpf_criador", "${Values.getCpf()}")
              json.put("cnpj_empresa", Values.getCnpj())
              json.put("dt_rota", data_completa)
              val connection = Connection(
                  ServicesUrl.getServiceURL(itemView.getContext(), ServicesUrl.SUBROTA), json,
                  Activity()
              )
              Log.e("Criado", json.toString())
              //enviar coisas e recuperar o json
              HttpPost().postWithVolley(connection)
              Log.e("rota adicionada", "rota")*/
        }
    }

    override fun onClick(view: View) {
    }

    var currentFragment: Fragment? = null

}