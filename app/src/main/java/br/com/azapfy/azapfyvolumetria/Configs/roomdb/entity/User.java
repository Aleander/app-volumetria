package br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity;

import android.arch.persistence.room.*;
import org.json.JSONException;
import org.json.JSONObject;

@Entity(indices = {@Index(value = {"cpf"}, unique = true)})
public class User {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "tipo")
    private String tipo;
    @ColumnInfo(name = "cpf")
    private String cpf;
    @ColumnInfo(name = "telefone")
    private String telefone;
    @ColumnInfo(name = "auth")
    private boolean auth;
    @ColumnInfo(name = "cnpj")
    private String cnpj;
    @ColumnInfo(name = "config")
    private String config;
    @ColumnInfo(name = "tipo_emp")
    public String tipo_emp ;
    @ColumnInfo(name = "grupo_emp")
    public String grupo_emp ;
    @ColumnInfo(name = "base")
    public String base ;
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public boolean getAuth() {
        return auth;
    }

    public void setAuth(boolean value) {
        this.auth = value;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(JSONObject config) {
        this.config = config.toString();
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public JSONObject getConfigAsJSON() throws JSONException {
        JSONObject config = new JSONObject(getConfig());
        return config;
    }

    @Ignore
    public String toString() {
        return ("@name:" + name + "@cpf:" + cpf + "@auth:" + auth +
                "@config:" + config);
    }

    @Ignore
    public boolean equals(User user) {
        return (this.name.equals(user.getName()) &&
                this.cpf.equals(user.getCpf()) &&
                this.auth == user.getAuth() &&
                this.config.equals(user.getConfig())
        );
    }






}