package br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index(value = { "_id"}, unique = true)})
public class Romaneio {
    @PrimaryKey(autoGenerate = true)
    public int id;


    @ColumnInfo(name = "nRomaneio")
    public String nRomaneio;

    @ColumnInfo(name = "data")
    public String data;
    @ColumnInfo(name = "limite")
    public int limite;
    @ColumnInfo(name = "valida")
    public int valida;
    @ColumnInfo(name = "quantidade")
    public int quantidade;
    @ColumnInfo(name = "quantidadeveri")
    public int quantidadeveri;
    @ColumnInfo(name = "situacao")
    public String situacao;
    @ColumnInfo(name = "cnpj")
    public String cnpj;
    @ColumnInfo(name = "cpf")
    public String cpf;
    @ColumnInfo(name = "_id")
    public String _id;
    @ColumnInfo(name = "porcentagem")
    public int porcentagem;
    @ColumnInfo(name = "embarcador")
    public String embarcador;
    @ColumnInfo(name = "tomador")
    public String tomador;
    @ColumnInfo(name = "updated_at")
    public String updated_at;
    @ColumnInfo(name = "base")
    public String base;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getnRomaneio() {
        return nRomaneio;
    }

    public void setnRomaneio(String nRomaneio) {
        this.nRomaneio = nRomaneio;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }

    public int getValida() {
        return valida;
    }

    public void setValida(int valida) {
        this.valida = valida;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getQuantidadeveri() {
        return quantidadeveri;
    }

    public void setQuantidadeveri(int quantidadeveri) {
        this.quantidadeveri = quantidadeveri;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getPorcentagem() {
        return porcentagem;
    }

    public void setPorcentagem(int porcentagem) {
        this.porcentagem = porcentagem;
    }

    public String getEmbarcador() {
        return embarcador;
    }

    public void setEmbarcador(String embarcador) {
        this.embarcador = embarcador;
    }

    public String getTomador() {
        return tomador;
    }

    public void setTomador(String tomador) {
        this.tomador = tomador;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
