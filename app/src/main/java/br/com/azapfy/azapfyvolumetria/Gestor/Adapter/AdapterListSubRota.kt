package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.SubRota
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.GestorComfirmar
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_subrota.view.*

class AdapterListSubRota(
    val items: MutableList<SubRota>,
    val context: Context?

) : RecyclerView.Adapter<ViewHolderSubRota>() {
    var rotas = ArrayList<SubRota>(items)
    override fun onBindViewHolder(p0: ViewHolderSubRota, p1: Int) {
        p0.data?.text = rotas.get(p1).date_create.replace("-", "/")
        p0.id = rotas.get(p1).id_rota
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderSubRota {
        return ViewHolderSubRota(
            LayoutInflater.from(context).inflate(
                R.layout.item_subrota,
                p0,
                false
            )
        )
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return rotas.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}

class ViewHolderSubRota(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {

    val data = itemView.datasubrota
    var id = ""


    init {
        itemView.setOnClickListener {
            Values.setPassTipo("rota")
            Values.setPassId(id)
            val intent = Intent(itemView.getContext(), GestorComfirmar::class.java)
            itemView.getContext().startActivity(intent)
        }


    }

    override fun onLongClick(v: View?): Boolean {

        return false
    }

    override fun onClick(view: View) {

    }

    companion object {
        private val numero = ArrayList<Int>()
    }


}