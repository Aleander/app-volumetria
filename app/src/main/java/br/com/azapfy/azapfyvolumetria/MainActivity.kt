package br.com.azapfy.azapfyvolumetria

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.FilterCod
import br.com.azapfy.azapfyvolumetria.Gestor.fragment.*
import br.com.azapfy.azapfyvolumetria.bipador.fragment.FragmentRotaPermanente
import br.com.azapfy.azapfyvolumetria.verificador.fragment.FragmentRomaneioPendente
import br.com.azapfy.azapfyvolumetria.verificador.fragment.FragmentRotasPendente
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.toolbar.*


class MainActivity : AppCompatActivity() {
    //Login usuario lucas 022.836.056.01
    var idList: ArrayList<String> = ArrayList()
    var currentScreen: Int? = null
    var currentFragment: Fragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.CRIAR, R.string.ROMANEIO)
        drawerLayout.addDrawerListener(toggle)
        toggle.setToolbarNavigationClickListener {
            Log.e("x", "ss")
        }
        toggle.syncState()
        if (Values.getModulo() == 1) {
            navView.getMenu().clear();
            navView.inflateMenu(R.menu.menu_bipador);
            Values.setUser("admbipador")
        }
        if (Values.getModulo() == 2) {
            navView.getMenu().clear();
            navView.inflateMenu(R.menu.menu_bipador);
            Values.setUser("admverificador")
        }
        val bottomNavigationBar = findViewById<BottomNavigationBar>(R.id.bottom_bar)
        val ROMANEIOADD = BottomBarItem(R.drawable.file, R.string.CRIAR)
        val ROTAS = BottomBarItem(R.drawable.roadcinza, R.string.ROTAS)
        val ROMANEIO = BottomBarItem(R.drawable.truckcinza, R.string.ROMANEIO)
        val ADICIONAR = BottomBarItem(R.drawable.clipboards, R.string.HISTORICO)
        val ROTASC = BottomBarItem(R.drawable.roadcinza, R.string.ROTAS)
        val ROMANEIOC = BottomBarItem(R.drawable.truckcinza, R.string.ROMANEIO)
        val VINCULARROMANEIO = BottomBarItem(R.drawable.vincular, R.string.VINCULAR)
        selectScreen(0)
        bottomNavigationBar.setOnSelectListener { position -> selectScreen(position) }
        if (Values.getUser() == "admin") {
            bottomNavigationBar.addTab(ROMANEIOADD)
            bottomNavigationBar.addTab(ROTAS)
            bottomNavigationBar.addTab(ROMANEIO)
            bottomNavigationBar.addTab(ADICIONAR)
        } else if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {

            navView.getMenu().clear();
            bottomNavigationBar.addTab(ROTASC)
            bottomNavigationBar.addTab(ROMANEIOC)
            if (Values.getUser() == "admverificador") {
                navView.inflateMenu(R.menu.menu_bipador);
            } else {
                navView.inflateMenu(R.menu.menu_delogar);
            }
        } else {

            navView.getMenu().clear();
            bottomNavigationBar.addTab(ROTASC)
            bottomNavigationBar.addTab(ROMANEIOC)
            if (Values.getUser() == "admbipador") {
                navView.inflateMenu(R.menu.menu_bipador);
            } else {
                navView.inflateMenu(R.menu.menu_delogar);
            }
        }
        navView.setNavigationItemSelectedListener(

            NavigationView.OnNavigationItemSelectedListener { item ->
                when (item.itemId) {

                    br.com.azapfy.azapfyvolumetria.R.id.gestor -> {
                        Values.setModulo(1)
                        Values.setUser("admbipador")
                        Thread {
                            AppDatabase.use(applicationContext).userDao().dropcodigo()
                        }.start()
                        var i = Intent(this@MainActivity, LoadingActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.criarusuario -> {
                        currentScreen = null
                        currentFragment = null
                        selectScreen(4)
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.verificador -> {
                        Values.setModulo(2)
                        Values.setUser("admverificador")
                        Thread {
                            AppDatabase.use(applicationContext).userDao().dropcodigo()
                        }.start()
                        var i = Intent(this@MainActivity, LoadingActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.bipador -> {
                        Values.setModulo(0)
                        Values.setUser("admbipador")
                        Thread {
                            AppDatabase.use(applicationContext).userDao().dropcodigo()
                        }.start()
                        var i = Intent(this@MainActivity, LoadingActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.pesquisa -> {
                        var i = Intent(this@MainActivity, FilterCod::class.java)
                        startActivity(i)
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.limparnav -> {
                        Thread {
                            AppDatabase.use(applicationContext).userDao().dropcodigo()
                            AppDatabase.use(applicationContext).userDao().dropromaneio()
                            AppDatabase.use(applicationContext).userDao().dropvolume()
                            AppDatabase.use(applicationContext).userDao().dropbase()
                            AppDatabase.use(applicationContext).userDao().dropsubrota()
                        }.start()
                        true
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.deslogar1 -> {
                        Thread {
                            AppDatabase.use(applicationContext).userDao().drop()
                            AppDatabase.use(applicationContext).userDao().dropcodigo()
                            AppDatabase.use(applicationContext).userDao().dropromaneio()
                            AppDatabase.use(applicationContext).userDao().dropvolume()
                            AppDatabase.use(applicationContext).userDao().dropbase()
                            AppDatabase.use(applicationContext).userDao().dropsubrota()
                        }.start()
                        var i = Intent(this@MainActivity, LoginActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                    br.com.azapfy.azapfyvolumetria.R.id.deslogarconfig -> {
                        Thread {
                            AppDatabase.use(applicationContext).userDao().drop()
                            AppDatabase.use(applicationContext).userDao().dropcodigo()
                            AppDatabase.use(applicationContext).userDao().dropromaneio()
                            AppDatabase.use(applicationContext).userDao().dropvolume()
                            AppDatabase.use(applicationContext).userDao().dropbase()
                            AppDatabase.use(applicationContext).userDao().dropsubrota()
                        }.start()
                        var i = Intent(this@MainActivity, LoginActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                }
                true
            })
    }

    private fun selectScreen(position: Int) {
        var newFragment: Fragment? = null
        currentScreen = position
        when (position) {
            ROMANEIOADD -> {
                if (Values.getUser() == "admin") {
                    newFragment = FragmentItensCreate()

                } else if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
                    newFragment = FragmentRotasPendente()

                } else {
                    newFragment = FragmentRotaPermanente()

                }
            }
            ROTAS -> {
                if (Values.getUser() == "admin") {
                    newFragment = FragmentRotasAdm()

                } else {
                    newFragment = FragmentRomaneioPendente()

                }
            }
            ROMANEIO -> {
                if (Values.getUser() == "admin") {
                    newFragment = FragmentRomaneioAdm()

                } else {
                    newFragment = FragmentRotasAdm()

                }
            }
            ADICIONAR -> {
                newFragment = FragmentHistorico()

            }
            USUARIO -> {
                newFragment = FragmentUsuarioCreate()

            }
        }
        loadFragment(newFragment!!)
    }

    fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            currentFragment = fragment
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, fragment)
                .commit()
            return true
        }
        return false
    }

    companion object {
        private const val ROMANEIOADD = 0
        private const val ROTAS = 1
        private const val ROMANEIO = 2
        private const val ADICIONAR = 3
        private const val USUARIO = 4
    }
}







