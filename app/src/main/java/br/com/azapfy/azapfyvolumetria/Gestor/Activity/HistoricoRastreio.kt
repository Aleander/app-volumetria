package br.com.azapfy.azapfyvolumetria.Gestor.Activity


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListHistoricoRastreio
import br.com.azapfy.azapfyvolumetria.MainActivity
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_list_rastreio.*
import org.json.JSONObject
import java.util.*


class HistoricoRastreio : AppCompatActivity() {
    // Initializing an empty ArrayList to be filled with dados
    lateinit var pass: User

    init {
        pass = User()
    }

    var teste = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_rastreio)
        lateinit var mAdapter: AdapterListHistoricoRastreio
        Log.e("tipo", Values.getPassTipo())
        rv_listconcluid.layoutManager = LinearLayoutManager(this)
        Thread {
            val codigos = mutableListOf<Codigo>()
            var list = AppDatabase.use(baseContext).codigoDao().findcodigo(Values.getPassId())
            if (list[0].cpfbipador != null && list[0].cpfbipador != "") {
                var newCodigo = Codigo()
                newCodigo.databipar = list[0].databipar
                newCodigo.cpfbipador = list[0].cpfbipador
                newCodigo.rota_bip = list[0].rota_bip
                newCodigo.numero = list[0].numero
                newCodigo.nome_bipador = list[0].nome_bipador
                codigos.add(newCodigo)
            }
            if (list[0].verificacao != null && list[0].verificacao != "") {
                var newCodigo = Codigo()
                newCodigo.rota_bip = list[0].rota_bip
                newCodigo.verificacao = list[0].verificacao
                codigos.add(newCodigo)
            }
            if (list[0].reconferido != null && list[0].reconferido != "") {
                var newCodigo = Codigo()
                if (list[0].cpfbipador != null && list[0].cpfbipador != "") {
                    newCodigo.rota_bip = list[0].rota_bip
                }else{
                    newCodigo.rota_bip == JSONObject(list[0].verificacao).getString("rota_veri")
                }
                newCodigo.reconferido = list[0].reconferido
                codigos.add(newCodigo)
            }
            runOnUiThread {
                mAdapter = AdapterListHistoricoRastreio(
                    codigos,
                    this
                )
                rv_listconcluid.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListHistoricoRastreio(ArrayList(), this)
        Values.setPassTipo("")
        this.voltar.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }
// Adds dados to the empty dados ArrayList
}
