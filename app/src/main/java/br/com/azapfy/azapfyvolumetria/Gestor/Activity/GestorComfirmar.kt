package br.com.azapfy.azapfyvolumetria.Gestor.Activity


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListHistoricoResu
import br.com.azapfy.azapfyvolumetria.MainActivity
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_list_completed.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class GestorComfirmar : AppCompatActivity() {
    // Initializing an empty ArrayList to be filled with dados
    lateinit var pass: User
    init {
        pass = User()
    }

    var teste = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_completed)
        lateinit var mAdapter: AdapterListHistoricoResu
        Log.e("tipo", Values.getPassTipo())
        rv_listconcluid.layoutManager = LinearLayoutManager(this)
        Thread {
            val list = AppDatabase.use(this).codigoDao().findcodigoSubRota(Values.getPassId())
            runOnUiThread {
                Log.e("list", list.size.toString())
                mAdapter = AdapterListHistoricoResu(
                    list,
                    this
                )
                rv_listconcluid.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListHistoricoResu(ArrayList(), this)
        Values.setPassTipo("")
        textcomplet.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mAdapter!!.filter(s.toString())
            }
        })
        GESTOR.setOnClickListener {
            Log.e("COMFIRMADO", "x")
            Thread {
                val json = JSONObject()
                try {
                    val data = Date()
                    val dateFormat_hora = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                    val cal = Calendar.getInstance()
                    cal.time = data
                    val data_atual = cal.time
                    val data_completa = dateFormat_hora.format(data_atual)
                    json.put("_id", Values.getPassId())
                    json.put("dt_confirmado", data_completa)
                    json.put("cpf_gestor", Values.getCpf())
                    json.put("situacao", "confirmado")
                    val connection = Connection(
                        ServicesUrl.getServiceURL(this, ServicesUrl.CONFIRMARGESTOR), json,
                        this
                    )
                    HttpPost().postWithVolley(connection)

                    AppDatabase.use(applicationContext).volumeDao()
                            .deleterota(Values.getPassId())

                    runOnUiThread() {
                        Toast.makeText(applicationContext, "ITEM SICRONIZADO.", Toast.LENGTH_SHORT).show()
                    }
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()

                } catch (f: Exception) {
                    LogController.e("sssd", "Error #3: $f")
                }
            }.start()
        }


        this.voltar.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }
// Adds dados to the empty dados ArrayList
}
