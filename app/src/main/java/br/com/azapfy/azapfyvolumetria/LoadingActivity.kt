package br.com.azapfy.azapfyvolumetria

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import kotlinx.android.synthetic.main.activity_loading.*

class LoadingActivity : AppCompatActivity() {
    private var i: Intent? = null
    private var progressBar: ProgressBar? = null
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_loading)
        Thread(Runnable {

            val users = AppDatabase.use(applicationContext).userDao().all
            for (u in users) {
                if (u.auth) {
                    Values.setTipo_emp(u.tipo_emp)
                    Values.setGroup_emp(u.grupo_emp)
                    Values.setUser(u.tipo)
                    Values.setCnpj(u.cnpj)
                    Values.setCpf(u.cpf)
                    Values.setTelefone(u.telefone)
                    Values.setNome(u.name)
                    i = Intent(this@LoadingActivity, MainActivity::class.java)

                    break
                }
            }
        }).start()
        VolleyGet.requestBases(
            "http://sede4.azapfy.com.br/api/grupo/empresa/listaBases/jc", this
        )
        VolleyGet.AtualizarRotas(this, applicationContext)
        VolleyGet.AtualizarCodigos(this, applicationContext, parent)
        progressBar = findViewById(R.id.porcload)
        //checkUpdates();
        YoYo.with(Techniques.BounceIn)
            .duration((SPLASH_TIME_OUT / 3).toLong())
            .repeat(0)
            .playOn(findViewById(R.id.ico))
        Handler().postDelayed({
            object : CountDownTimer(1300, 10) {
                override fun onTick(millisUntilFinished: Long) {
                    porceload.progress = ((100 * (1300 - millisUntilFinished)) / 1300).toInt()
                }

                override fun onFinish() {
                    porceload.progress = 100
                }
            }.start()
        }, (SPLASH_TIME_OUT / 3).toLong())
        Handler().postDelayed({
            startActivity()
        }, (SPLASH_TIME_OUT - 100).toLong())
    }

    fun startActivity() {
        if (i == null) {
            i = Intent(this@LoadingActivity, LoginActivity::class.java)
        }
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i!!.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(i)
    }
    companion object {
        private val SPLASH_TIME_OUT = 3000
    }
}
