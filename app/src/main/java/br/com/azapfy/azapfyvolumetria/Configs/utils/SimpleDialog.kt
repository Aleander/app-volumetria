package br.com.azapfy.azapfyvolumetria.Configs.utils

import android.app.Activity
import android.app.AlertDialog
import android.util.Log

object SimpleDialog {
    private const val TAG = "Class$0"
    const val NO_INTERNET = 0

    fun show(ctx: Activity?, e: String) {
        if (ctx == null) {
            return
        }
        ctx.runOnUiThread {
            try {
                val builder = AlertDialog.Builder(ctx)
                builder.setMessage(e)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setNegativeButton("OK") { dialog, id -> dialog.cancel() }

                val alert = builder.create()
                alert?.show()
            } catch (e: Exception) {
                Log.e(TAG, "Error #1: $e")
            }
        }
    }

    fun show(ctx: Activity, code: Int) {
        when (code) {
            NO_INTERNET -> show(
                ctx,
                "Oops! Não há conexão com a internet. Verifique sua conexão e tente " + "novamente."
            )
            else -> {
            }
        }
    }
}
