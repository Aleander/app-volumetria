package br.com.azapfy.azapfyvolumetria.Configs.constants;

import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo;

import java.util.ArrayList;

public class Values {


    public static String group_emp;

    public static String tipo_emp;

    public static String telefone;

    public static String nome;

    public static String passromaneio;

    public static String User;

    public static int Modulo;

    public static String Descricao;

    public static int Quantidadebipadas;

    public static String passId;

    public static String databip;

    public static String cnpj;

    public static String PassCodigo;

    public static String cpf;

    public static String vincularRota;

    public static ArrayList<Codigo> codigo;

    public static Codigo codigoFilter;

    public static ArrayList<String> arraySelection;

    public static String passRota;

    public static String passTipo;

    public static ArrayList<Codigo> items;

    public static String getGroup_emp() {
        return group_emp;
    }

    public static void setGroup_emp(String group_emp) {
        Values.group_emp = group_emp;
    }

    public static String getTipo_emp() {
        return tipo_emp;
    }

    public static void setTipo_emp(String tipo_emp) {
        Values.tipo_emp = tipo_emp;
    }

    public static String getTelefone() {
        return telefone;
    }

    public static void setTelefone(String telefone) {
        Values.telefone = telefone;
    }

    public static String getNome() {
        return nome;
    }

    public static void setNome(String nome) {
        Values.nome = nome;
    }

    public static String getPassromaneio() {
        return passromaneio;
    }

    public static void setPassromaneio(String passromaneio) {
        Values.passromaneio = passromaneio;
    }

    public static String getUser() {
        return User;
    }

    public static void setUser(String user) {
        User = user;
    }

    public static int getModulo() {
        return Modulo;
    }

    public static void setModulo(int modulo) {
        Modulo = modulo;
    }

    public static String getDescricao() {
        return Descricao;
    }

    public static void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public static int getQuantidadebipadas() {
        return Quantidadebipadas;
    }

    public static void setQuantidadebipadas(int quantidadebipadas) {
        Quantidadebipadas = quantidadebipadas;
    }

    public static String getPassId() {
        return passId;
    }

    public static void setPassId(String passId) {
        Values.passId = passId;
    }

    public static String getDatabip() {
        return databip;
    }

    public static void setDatabip(String databip) {
        Values.databip = databip;
    }

    public static String getCnpj() {
        return cnpj;
    }

    public static void setCnpj(String cnpj) {
        Values.cnpj = cnpj;
    }

    public static String getPassCodigo() {
        return PassCodigo;
    }

    public static void setPassCodigo(String passCodigo) {
        PassCodigo = passCodigo;
    }

    public static String getCpf() {
        return cpf;
    }

    public static void setCpf(String cpf) {
        Values.cpf = cpf;
    }

    public static String getVincularRota() {
        return vincularRota;
    }

    public static void setVincularRota(String vincularRota) {
        Values.vincularRota = vincularRota;
    }

    public static ArrayList<Codigo> getCodigo() {
        return codigo;
    }

    public static void setCodigo(ArrayList<Codigo> codigo) {
        Values.codigo = codigo;
    }

    public static Codigo getCodigoFilter() {
        return codigoFilter;
    }

    public static void setCodigoFilter(Codigo codigoFilter) {
        Values.codigoFilter = codigoFilter;
    }

    public static ArrayList<String> getArraySelection() {
        return arraySelection;
    }

    public static void setArraySelection(ArrayList<String> arraySelection) {
        Values.arraySelection = arraySelection;
    }

    public static String getPassRota() {
        return passRota;
    }

    public static void setPassRota(String passRota) {
        Values.passRota = passRota;
    }

    public static String getPassTipo() {
        return passTipo;
    }

    public static void setPassTipo(String passTipo) {
        Values.passTipo = passTipo;
    }

    public static ArrayList<Codigo> getItems() {
        return items;
    }

    public static void setItems(ArrayList<Codigo> items) {
        Values.items = items;
    }
}
