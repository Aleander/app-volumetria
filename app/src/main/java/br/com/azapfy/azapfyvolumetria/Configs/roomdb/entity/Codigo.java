package br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index(value = {"id", "numero"}, unique = true)})
public class Codigo {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "numero")
    public String numero;

    @ColumnInfo(name = "descricao")
    public String descricao;

    @ColumnInfo(name = "descricaoveri")
    public String descricaoveri;

    @ColumnInfo(name = "subrota_bip")
    public String subrota_bip;

    @ColumnInfo(name = "subrota_veri")
    public String subrota_veri;

    @ColumnInfo(name = "rota_bip")
    public String rota_bip;

    @ColumnInfo(name = "rota_veri")
    public String rota_veri;

    @ColumnInfo(name = "romaneio_bip")
    public String romaneio_bip;

    @ColumnInfo(name = "romaneio_veri")
    public String romaneio_veri;

    @ColumnInfo(name = "_id")
    public String _id;

    @ColumnInfo(name = "cpfbipador")
    public String cpfbipador;

    @ColumnInfo(name = "cpfveri")
    public String cpfveri;

    @ColumnInfo(name = "databipar")
    public String databipar;

    @ColumnInfo(name = "dataveri")
    public String dataveri;

    @ColumnInfo(name = "situacao")
    public String situacao;

    @ColumnInfo(name = "telefone_bipador")
    public String telefone_bipador;

    @ColumnInfo(name = "nome_bipador")
    public String nome_bipador;

    @ColumnInfo(name = "reconferido")
    public String reconferido;

    @ColumnInfo(name = "verificacao")
    public String verificacao;

    @ColumnInfo(name = "cnpj")
    public String cnpj;

    @ColumnInfo(name = "erro")
    public Boolean erro;

    @ColumnInfo(name = "pendente")
    public Boolean pendente;

    @ColumnInfo(name = "aprovado")
    public Boolean aprovado;

    public Boolean getErro() {
        return erro;
    }

    public void setErro(Boolean erro) {
        this.erro = erro;
    }

    public int pass;

    public String getTelefone_bipador() {
        return telefone_bipador;
    }

    public void setTelefone_bipador(String telefone_bipador) {
        this.telefone_bipador = telefone_bipador;
    }

    public String getNome_bipador() {
        return nome_bipador;
    }

    public void setNome_bipador(String nome_bipador) {
        this.nome_bipador = nome_bipador;
    }

    public String getReconferido() {
        return reconferido;
    }

    public void setReconferido(String reconferido) {
        this.reconferido = reconferido;
    }

    public String getVerificacao() {
        return verificacao;
    }

    public void setVerificacao(String verificacao) {
        this.verificacao = verificacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSubrota_bip() {
        return subrota_bip;
    }

    public void setSubrota_bip(String subrota_bip) {
        this.subrota_bip = subrota_bip;
    }

    public String getSubrota_veri() {
        return subrota_veri;
    }

    public void setSubrota_veri(String subrota_veri) {
        this.subrota_veri = subrota_veri;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricaoveri() {
        return descricaoveri;
    }

    public void setDescricaoveri(String descricaoveri) {
        this.descricaoveri = descricaoveri;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getRota_bip() {
        return rota_bip;
    }

    public void setRota_bip(String rota_bip) {
        this.rota_bip = rota_bip;
    }

    public String getRota_veri() {
        return rota_veri;
    }

    public void setRota_veri(String rota_veri) {
        this.rota_veri = rota_veri;
    }

    public String getRomaneio_bip() {
        return romaneio_bip;
    }

    public void setRomaneio_bip(String romaneio_bip) {
        this.romaneio_bip = romaneio_bip;
    }

    public String getRomaneio_veri() {
        return romaneio_veri;
    }

    public void setRomaneio_veri(String romaneio_veri) {
        this.romaneio_veri = romaneio_veri;
    }

    public String getCpfbipador() {
        return cpfbipador;
    }

    public void setCpfbipador(String cpfbipador) {
        this.cpfbipador = cpfbipador;
    }

    public String getCpfveri() {
        return cpfveri;
    }

    public void setCpfveri(String cpfveri) {
        this.cpfveri = cpfveri;
    }

    public String getDatabipar() {
        return databipar;
    }

    public void setDatabipar(String databipar) {
        this.databipar = databipar;
    }

    public String getDataveri() {
        return dataveri;
    }

    public void setDataveri(String dataveri) {
        this.dataveri = dataveri;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public int getPass() {
        return pass;
    }

    public void setPass(int pass) {
        this.pass = pass;
    }

    public Boolean getPendente() {
        return pendente;
    }

    public void setPendente(Boolean pendente) {
        this.pendente = pendente;
    }

    public Boolean getAprovado() {
        return aprovado;
    }

    public void setAprovado(Boolean aprovado) {
        this.aprovado = aprovado;
    }
}
