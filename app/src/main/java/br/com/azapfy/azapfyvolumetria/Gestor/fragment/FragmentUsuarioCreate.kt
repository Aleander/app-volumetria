package br.com.azapfy.azapfyvolumetria.Gestor.fragment


import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.R
import com.github.rtoshiro.util.format.SimpleMaskFormatter
import com.github.rtoshiro.util.format.text.MaskTextWatcher
import kotlinx.android.synthetic.main.activity_usuario_create.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class FragmentUsuarioCreate : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_usuario_create, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val smf = SimpleMaskFormatter("NNN.NNN.NNN-NN")
        val smfTelefone = SimpleMaskFormatter("(NN)NNNN-NNNN")
        val mtw = MaskTextWatcher(cpf, smf)
        val mtwTelefone = MaskTextWatcher(telefone, smfTelefone)
        cpf!!.addTextChangedListener(mtw)
        telefone!!.addTextChangedListener(mtwTelefone)
        criar_usuario.setOnClickListener {
            if (nome_usuario.text.toString() == "" || email.text.toString() == ""
                || cpf.text.toString() == "" || telefone.text.toString() == ""
                || tipouser.text.toString() == "Tipo de Usuário" || nome_base.text.toString() == "Base"
            ) {
                Toast.makeText(requireContext(), "Por favor preencha os campos", Toast.LENGTH_SHORT).show()
            } else {

                val data = Date()
                val dateFormat_hora = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                val cal = Calendar.getInstance()
                cal.time = data
                val data_atual = cal.time
                val data_completa = dateFormat_hora.format(data_atual)
                val json = JSONObject()
                val cpfMaskRemoved = cpf!!.text.toString()
                    .replace(".", "")
                    .replace("-", "")
                Thread {
                    try {
                        json.put("name", nome_usuario.text.toString())
                        json.put("email", email.text.toString())
                        json.put("cpf", cpfMaskRemoved.toString())
                        json.put("telefone", telefone.text.toString())
                        if (tipouser.text == "Gestor") {
                            json.put("grupo_usuario", "admin")
                        } else if (tipouser.text == "Verificador") {
                            json.put("grupo_usuario", "verificador")
                        } else {
                            json.put("grupo_usuario", "bipador")
                        }

                        json.put("password", senha.text.toString())
                        json.put("grupo_emp", "${Values.getGroup_emp()}")
                        json.put("cnpj_empresa", "${Values.getCnpj()}")
                        json.put("base", nome_base.text)
                        Log.e("Criado", json.toString())
                        val connection = Connection(
                            ServicesUrl.getServiceURL(requireContext(), ServicesUrl.CRIARUSER), json,
                            requireActivity()
                        )
                        HttpPost().postHist(connection)

                        activity!!.runOnUiThread {
                            Toast.makeText(requireContext(), "CRIADO COM SUCESSO.", Toast.LENGTH_SHORT).show()
                            nome_usuario.setText("")
                            email.setText("")
                            cpf.setText("")
                            telefone.setText("")
                        }
                    } catch (f: Exception) {
                        activity!!.runOnUiThread {
                            Toast.makeText(requireContext(), "ERRO: $f", Toast.LENGTH_SHORT).show()
                        }
                        LogController.e("sssd", "Error #3: $f")

                    }
                }.start()
            }
        }
        button_alterar_base.setOnClickListener {
            showDialog()
        }

        button_alterar_usuario.setOnClickListener {
            showDialogUsuario()
        }
    }

    private fun showDialog() {

        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Selecione a Base")
        var arrayBases = java.util.ArrayList<String>()
        Thread {
            VolleyGet.requestBases(
                "http://sede4.azapfy.com.br/api/grupo/empresa/listaBases/jc", activity
            )
            var bases = AppDatabase.use(context).baseDao().findbase()
            for (i in 0 until bases.size) {
                arrayBases.add(bases.get(i).toString())
            }
            Log.e("arrry", arrayBases.toString())
            activity?.runOnUiThread {
                builder.setSingleChoiceItems(arrayBases.toTypedArray(), -1) { _, which ->
                    nome_base.text = arrayBases[which]
                    dialog.dismiss()
                }
                dialog = builder.create()
                dialog.show()
            }
        }.start()
    }

    private fun showDialogUsuario() {

        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Tipo de Usuário ")
        var arrayBases = java.util.ArrayList<String>()
        arrayBases.add("Gestor")
        arrayBases.add("Bipador")
        arrayBases.add("Verificador")
        builder.setSingleChoiceItems(arrayBases.toTypedArray(), -1) { _, which ->
            tipouser.text = arrayBases[which]
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()


    }
}
