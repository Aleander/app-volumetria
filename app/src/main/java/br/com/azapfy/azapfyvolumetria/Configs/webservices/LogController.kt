package br.com.azapfy.azapfymotoristas.utils.webservices

import android.util.Log

object LogController {
    private val ACTIVATE_LOGS = true
    fun e(tag: String, message: String) {
        if (ACTIVATE_LOGS) {
            Log.e(tag, message)
        }
    }
}