package br.com.azapfy.azapfyvolumetria.Gestor.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.Configs.utils.SimpleDialog
import br.com.azapfy.azapfyvolumetria.Configs.webservices.AvailableNetwork
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_fragment_itens_create.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class FragmentItensCreate : Fragment() {
    var tipoCriacao = "romaneio"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mView = inflater.inflate(R.layout.activity_fragment_itens_create, container, false)

        return mView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var tipo = "x"

        Thread {
            val users = AppDatabase.use(requireContext()).userDao().all
            for (u in users) {
                if (u.tipo_emp == "transp") {
                    tipo = "Embarcador"
                }
                if (u.tipo_emp == "dist") {
                    tipo = "Tomador"
                }
            }
            activity!!.runOnUiThread {
                Log.e("tipo", tipo)
                nome!!.hint = tipo
            }
        }.start()

        button_alterar.setOnClickListener {
            when (tipoCriacao) {
                "romaneio" -> {

                    tipoCriacao = "rota"
                    ocorrencia.text = "Criar Rota"
                    tipo_separator.visibility = View.GONE
                    numromaneio.visibility = View.GONE
                    numromaneio_separator.visibility = View.GONE
                    nome_rota.visibility = View.VISIBLE
                    nome_rota_separator.visibility = View.VISIBLE
                    nome.visibility = View.GONE
                }
                "rota" -> {

                    tipoCriacao = "romaneio"
                    ocorrencia.text = "Criar Romaneio"
                    tipo_separator.visibility = View.VISIBLE
                    numromaneio.visibility = View.VISIBLE
                    numromaneio_separator.visibility = View.VISIBLE
                    nome.visibility = View.VISIBLE
                    nome_rota.visibility = View.GONE
                    nome_rota_separator.visibility = View.GONE

                }
            }
        }
        button_alterar_base.setOnClickListener {
            Thread {
                VolleyGet.requestBases(
                    "http://sede4.azapfy.com.br/api/grupo/empresa/listaBases/jc", requireActivity()
                )
                requireActivity().runOnUiThread {
                    showDialog()
                }
            }.start()
        }
        button_finalizar!!.setOnClickListener {

            val data = Date()
            val dateFormat_hora = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
            val cal = Calendar.getInstance()
            cal.time = data
            val data_atual = cal.time
            val data_completa = dateFormat_hora.format(data_atual)
            val json = JSONObject()
            if (AvailableNetwork.isNetworkAvaliable(requireContext(), 0) == "none") {
                SimpleDialog.show(
                    requireActivity(), "Oops! Não há conexão com a internet. " +
                            "Verifique" +
                            " sua conexão e tente novamente."
                )
                return@setOnClickListener
            } else if (tipoCriacao == "romaneio") {
                if (nome.text.toString() == "" || numromaneio.text.toString() == ""
                    || nome_base.text.toString() == "Base"
                ) {
                    Toast.makeText(requireContext(), " Por favor preencha os campos", Toast.LENGTH_SHORT).show()
                } else {
                    Thread {
                        try {
                            val users = AppDatabase.use(requireContext()).userDao().all
                            for (u in users) {
                                if (u.tipo_emp == "transp") {

                                    json.put("tomador", nome.text)

                                }
                                if (u.tipo_emp == "dist") {
                                    json.put("embarcador", nome.text)
                                }
                            }
                            json.put("numero_romaneio", numromaneio.text.toString())
                            json.put("cpf_criador", "${Values.getCpf()}")
                            json.put("grupo_emp", "${Values.getGroup_emp()}")
                            json.put("cnpj_empresa", "${Values.getCnpj()}")
                            if (nome_base.text != "") {
                                json.put("base", nome_base.text)
                            }
                            json.put("situacao", "cadastrado")
                            val connection = Connection(
                                ServicesUrl.getServiceURL(requireContext(), ServicesUrl.ROMANEIOCREATE), json,
                                requireActivity()
                            )
                            Log.e("Criado", json.toString())
                            //enviar coisas e recuperar o json
                            HttpPost().postWithVolley(connection)
                            Log.e("rota adicionada", "rota")
                            VolleyGet.AtualizarRotas(requireActivity(), requireContext())
                            activity!!.runOnUiThread {
                                nome_rota.setText("")
                                nome.setText("")

                            }
                        } catch (f: Exception) {
                            LogController.e("sssd", "Error #3: $f")
                            activity!!.runOnUiThread {
                                nome_rota.setText("")
                                nome.setText("")
                                Toast.makeText(context, "ERRO." + f, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }.start()
                }
            } else if (tipoCriacao == "rota") {
                if (nome_rota.text.toString() == "" || nome_base.text.toString() == "Base") {
                    Toast.makeText(requireContext(), " Por favor preencha os campos", Toast.LENGTH_SHORT).show()
                } else {
                    Thread {
                        try {
                            val users = AppDatabase.use(requireContext()).userDao().all
                            for (u in users) {
                                if (u.tipo_emp == "transp") {

                                    json.put("tomador", u.grupo_emp)
                                }
                                if (u.tipo_emp == "dist") {
                                    json.put("embarcador", u.grupo_emp)
                                }
                            }
                            // json.put("created_at", data_completa)
                            json.put("rota", nome_rota.text.toString())
                            json.put("quantidade_codigos", 0)
                            json.put("cpf_criador", "${Values.getCpf()}")
                            json.put("grupo_emp", "${Values.getGroup_emp()}")
                            json.put("cnpj_empresa", "${Values.getCnpj()}")
                            json.put("base", nome_base.text)
                            json.put("situacao", "cadastrado")
                            val connection = Connection(
                                ServicesUrl.getServiceURL(requireContext(), ServicesUrl.ROTACREATE), json,
                                requireActivity()
                            )
                            Log.e("Criado", json.toString())
                            //enviar coisas e recuperar o json
                            HttpPost().postWithVolley(connection)
                            Log.e("rota adicionada", "rota")
                            VolleyGet.AtualizarRotas(requireActivity(), requireContext())
                            activity!!.runOnUiThread {
                                nome_rota.setText("")
                                nome.setText("")
                                Toast.makeText(context, "CRIADO COM SUCESSO.", Toast.LENGTH_SHORT).show()
                            }
                        } catch (f: Exception) {
                            LogController.e("sssd", "Error #3: $f")
                            activity!!.runOnUiThread {
                                nome_rota.setText("")
                                nome.setText("")
                                Toast.makeText(context, "ERRO." + f, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }.start()
                }
            }
        }
    }
    private fun showDialog() {
        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Selecione a Base")
        var arrayBases = ArrayList<String>()
        Thread {
            var bases = AppDatabase.use(context).baseDao().findbase()
            for (i in 0 until bases.size) {
                arrayBases.add(bases.get(i).toString())
            }
            Log.e("arrry", arrayBases.toString())
            activity?.runOnUiThread {
                builder.setSingleChoiceItems(arrayBases.toTypedArray(), -1) { _, which ->
                    nome_base.text = arrayBases[which]
                    dialog.dismiss()
                }
                dialog = builder.create()
                dialog.show()
            }
        }.start()
    }


}

