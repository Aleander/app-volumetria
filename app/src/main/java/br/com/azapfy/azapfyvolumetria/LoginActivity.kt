package br.com.azapfy.azapfyvolumetria

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.controller.DbController
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.Configs.utils.SimpleDialog
import br.com.azapfy.azapfyvolumetria.Configs.webservices.AvailableNetwork
import com.github.rtoshiro.util.format.SimpleMaskFormatter
import com.github.rtoshiro.util.format.text.MaskTextWatcher
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.util.*

class LoginActivity : AppCompatActivity() {
    private var cpf: EditText? = null
    private var senha: EditText? = null
    private var cpfLogin = ""
    private var devClicksCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /** Remove o título da janela  */
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_login)
        /** Define as variáveis de ambiente  */
        cpf = findViewById(R.id.cpfMotorista)
        senha = findViewById(R.id.senha)
        val smf = SimpleMaskFormatter("NNN.NNN.NNN-NN")
        val mtw = MaskTextWatcher(cpf, smf)
        cpf!!.addTextChangedListener(mtw)
        image_logo_azapfy.setOnClickListener {
            devClicksCount++
            if (devClicksCount > 16) {
                Toast.makeText(
                    this,
                    "Você está a ${20 - devClicksCount} passos de ativar o modo desenvolvedor",
                    Toast.LENGTH_SHORT
                ).show()
                if (devClicksCount > 19) {
                    devClicksCount = 0
                    createDevConfigs()
                }
            }
        }
    }

    private fun createDevConfigs() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("URL do Desenvolvedor")
        val input = EditText(this)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        input.layoutParams = lp
        alertDialog.setView(input)
        alertDialog.setPositiveButton(
            "Confirmar"
        ) { _, _ ->
            val finalURL: String = when (input.text.toString()) {
                "padrao" -> br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_PADRAO
                "sede2" -> br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_SEDE2
                "homolog" -> br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_HOMOLOG
                "local1" -> br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_LOCAL
                else -> {
                    when {
                        input.text.toString().contains("sede") -> "http://${input.text}.azapfy.com.br/api/android/"
                        input.text.toString().contains("***") -> "http://${(input.text.toString()).removeSuffix("***")}.azapfy.com.br/api/android/"
                        else -> input.text.toString()
                    }
                }
            }
            Toast.makeText(this, "Atualizado com sucesso! A nova URL é $finalURL", Toast.LENGTH_SHORT).show()
            Thread {
                br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_LOCAL = finalURL
            }.start()
        }
        alertDialog.show()
    }

    fun onLogin(v: View) {
        val cpfMaskRemoved = cpf!!.text.toString()
            .replace(".", "")
            .replace("-", "")

        cpfLogin = cpfMaskRemoved
        // Verifica a conexão com a internet
        if (AvailableNetwork.isNetworkAvaliable(this@LoginActivity, 0) == "none") {
            SimpleDialog.show(
                this@LoginActivity, "Oops! Não há conexão com a internet. " +
                        "Verifique" +
                        " sua conexão e tente novamente."
            )
            return
        }
        // Verifica se o CPF tem 11 dígitos
        if (cpfMaskRemoved.length < 11) {
            SimpleDialog.show(this, "O CPF precisa ter 11 dígitos.")
            return
        }
        // Verifica se o CPF existe
        Log.e("cpf" , cpfMaskRemoved)
        if (!br.com.azapfy.azapfyvolumetria.Configs.utils.ValidaCPF.isCPF(cpfMaskRemoved)) {
            SimpleDialog.show(this, "O CPF digitado não existe.")
            return
        }
        // Solicita permissões de uso do aplicativo
        if (!checkAndRequestPermissions()) {
            SimpleDialog.show(this, "Não é possível prosseguir sem as permissões " + "necessárias.")
            return
        }
        pass(cpfMaskRemoved)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        for (permission in grantResults) {
            if (permission != PackageManager.PERMISSION_GRANTED) {
                return
            }
        }

        pass(cpfLogin)
    }

    private fun checkAndRequestPermissions(): Boolean {
        val permissionCamera = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val listPermissionsNeeded = ArrayList<String>()
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(), 0
            )
            return false
        }
        return true
    }


    private fun pass(cpfMaskRemoved: String) {
        Thread(Runnable {
            val e: String? = null
            val user = JSONObject()
            try {
                user.put("cpf", cpfMaskRemoved)
                user.put("password", senha!!.text.toString())
                user.put("imei", DbController.getIMEI(this@LoginActivity))
                val connection = Connection(
                    ServicesUrl.getServiceURL(applicationContext, ServicesUrl.VALIDATE), user,
                    this@LoginActivity
                )
                //enviar coisas e recuperar o json
                HttpPost().postWithVolley(connection)

                //HttpPost().execute(connection)
            } catch (f: Exception) {
                LogController.e(TAG, "Error #3: $f")
            }
            /*
                    //e = "Usuário não cadastrado.";
                    Intent i = new Intent(LoginActivity.this, MainActivity.class)
                            .putExtra("user", u.getCpf())
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);
                    */
        }).start()

    }

    companion object {

        private val RC_CAMERA_AND_LOCATION = 1
        private val TAG = "Activity$1"
    }
}
