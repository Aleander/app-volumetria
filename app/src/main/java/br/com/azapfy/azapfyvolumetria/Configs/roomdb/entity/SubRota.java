package br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index(value = {"id" , "id_rota" }, unique = true)})
public class SubRota {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "rota")
    public String rota;
    @ColumnInfo(name = "id_rota")
    public String id_rota;
    @ColumnInfo(name = "date_create")
    public String date_create;
}