package br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao;

import android.arch.persistence.room.*;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo;

import java.util.List;


@Dao
public
interface CodigoDao {

    @Insert
    void insert(Codigo codigo);

    @Update
    void update(Codigo codigo);

    @Delete
    void delete(Codigo codigo);

    @Query("DELETE  FROM  Codigo  WHERE  numero = :codigo")
    void Deletecodi(String codigo);


    @Query("DELETE  FROM  Codigo  WHERE  rota_bip = :rota AND pendente = :pen ")
    void deleteall(String rota, Boolean pen);

    @Query("DELETE  FROM  Codigo  WHERE  romaneio_bip = :rota AND pendente = :pen ")
    void deleteallRomaneio(String rota, Boolean pen);

    @Query("UPDATE Codigo SET numero = :numero, cpfveri = :cpf, dataveri = :data_veri" +
            " ,rota_veri = :rota, descricaoveri = :descri , erro = 0 , situacao = 'verificada'  , pendente = :pen  , _id = :id WHERE numero = :numero")
    void updatecodigos(String numero, String cpf, String data_veri, String rota, String descri, Boolean pen, String id);

    @Query("UPDATE Codigo SET  aprovado = :pen , pendente = :pen WHERE numero = :numero ")
    void updatecodigosAprovadoBipa(Boolean pen, String numero);

    @Query("UPDATE Codigo SET  aprovado = :pen , pendente = :pen WHERE numero = :numero ")
    void updatecodigosAprovadoVeri(Boolean pen, String numero);

    @Query("UPDATE Codigo SET pendente = :pen WHERE pendente = :relative")
    void updateCodigosPendentes(Boolean pen, Boolean relative);

    @Query("SELECT  * FROM Codigo  WHERE subrota_bip = :id ")
    List<Codigo> findcodigoSubRota(String id);

    @Query("SELECT  rota_bip FROM Codigo  WHERE numero = :numero ")
    String findcodigorota(String numero);

    @Query("SELECT * FROM Codigo WHERE numero = :numero ")
    List<Codigo> findcodigo(String numero);

    @Query("SELECT * FROM Codigo WHERE erro = :erro AND aprovado != :aprovado")
    List<Codigo> finderroNoAproved(Boolean erro, Boolean aprovado);

    @Query("SELECT * FROM Codigo WHERE erro = :erro  AND aprovado != :aprovado  AND rota_veri IS NULL AND rota_bip IS NOT NULL")
    List<Codigo> finderroNoVeri(Boolean erro, Boolean aprovado);

    @Query("SELECT * FROM Codigo WHERE erro = :erro  AND aprovado != :aprovado  AND rota_bip IS NULL  AND rota_veri IS NOT NULL")
    List<Codigo> finderroNoBip(Boolean erro, Boolean aprovado);


    @Query("SELECT * FROM Codigo WHERE erro = :erro  AND aprovado != :aprovado  AND rota_bip != rota_veri  AND rota_bip is NOT NULL")
    List<Codigo> finderroRotaErro(Boolean erro, Boolean aprovado);

    @Query("SELECT  numero FROM Codigo  WHERE numero = :numero ")
    String findveriNumero(String numero);

    @Query("SELECT  * FROM Codigo WHERE _id = :x ")
    List<Codigo> findcodigos(String x);



    @Query("DELETE  FROM  Codigo  WHERE  _id = :id AND numero = :numero")
    void detelecodigo(String id, String numero);

    @Query("SELECT count(*) FROM Codigo WHERE  _id = :id")
    String findquant(String id);

    @Query("SELECT  * FROM Codigo WHERE pendente = :x AND numero = :numero ")
    List<Codigo> findcodigosPendeBipador(Boolean x, String numero);

    //Modulo verificador


    @Query("SELECT  rota_veri FROM Codigo  WHERE numero = :numero ")
    String findcodigorotaVeri(String numero);

    @Query("SELECT  numero FROM Codigo WHERE pendente = :x AND numero = :numero ")
    String findcodigosPendeVeri(Boolean x, String numero);

    @Query("SELECT count(*) FROM Codigo WHERE  pendente  = :pen AND rota_bip = :rota  ")
    String findquantveriPendBipa(Boolean pen, String rota);

    @Query("SELECT count(*) FROM Codigo WHERE  pendente  = :pen AND rota_veri = :rota  ")
    String findquantveriPendVeri(Boolean pen, String rota);

    @Query("SELECT  numero FROM Codigo  WHERE numero = :numero  AND rota_bip = :rota")
    String findverirotanumero(String numero, String rota);

    @Query("SELECT  * FROM Codigo WHERE pendente = :x ")
    List<Codigo> findcodigosPende(Boolean x);

    @Query("SELECT  numero FROM Codigo  WHERE numero = :numero")
    String findverificador(String numero);


}
