package br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Base;

import java.util.List;


@Dao
public interface BaseDao {

    @Insert
    void insert(Base base);

    @Query("SELECT * FROM Base ")
    List<Base> finall();
    @Query("SELECT base FROM Base ")
    List<String> findbase();
    @Query("SELECT base FROM Base WHERE  base = :base")
    String findBaseVeri(String base);
}
