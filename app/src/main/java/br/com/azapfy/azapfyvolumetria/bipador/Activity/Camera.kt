package br.com.azapfy.azapfyvolumetria.bipador.Activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.EditText
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.MainActivity
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.fragment.FragmentEscaner
import br.com.azapfy.azapfyvolumetria.bipador.fragment.FragmentListNew
import kotlinx.android.synthetic.main.activity_camera.*

class Camera : AppCompatActivity() {
    var currentFragment: Fragment? = null
    lateinit var pass: User
    var fragmentrequisition = 0
    init {
        pass = User()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_camera)
        var newFragment: Fragment?
        newFragment = FragmentEscaner()
        loadFragment(newFragment!!)
        relative.visibility = View.GONE
        relativetwo.visibility = View.VISIBLE
        this.voltar.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }
        this.descriadd.setOnClickListener {
            val input = EditText(this)
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Adicionar Observação")
            input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_DATETIME_VARIATION_NORMAL
            builder.setView(input)
            builder.setPositiveButton("OK",
                DialogInterface.OnClickListener { dialog, which ->
                    Log.e("descricao", input.text.toString())
                    Values.setDescricao(input.text.toString())
                }
            )
            builder.setNegativeButton("Cancel",
                DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
            builder.show()
        }
        this.esca.setOnClickListener {
            if(fragmentrequisition == 0) {
                fragmentrequisition = 1
                relative.visibility = View.GONE
                relativetwo.visibility = View.VISIBLE
                newFragment = FragmentEscaner()
                loadFragment(newFragment!!)
            }else{
                fragmentrequisition = 0
                relative.visibility = View.VISIBLE
                relativetwo.visibility = View.GONE
                newFragment = BarcodeReaderFragment()
                loadFragment(newFragment!!)
            }
        }
        this.finali.setOnClickListener {

            val i = Intent(this, FragmentListNew::class.java)
            startActivity(i)
        }
        this.finalitwo.setOnClickListener {
            val i = Intent(this, FragmentListNew::class.java)
            startActivity(i)
        }
    }

    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            currentFragment = fragment
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
            return true
        }
        return false
    }
}
