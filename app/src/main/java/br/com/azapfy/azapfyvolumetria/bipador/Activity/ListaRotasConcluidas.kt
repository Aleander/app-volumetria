package br.com.azapfy.azapfyvolumetria.bipador.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.MainActivity
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Adapter.AdapterListRotaConcluidas
import kotlinx.android.synthetic.main.activity_fragment_rota_concluidas.*
import kotlinx.android.synthetic.main.activity_fragment_rota_permanentes.aturotaPermanente
import kotlinx.android.synthetic.main.activity_fragment_rota_permanentes.rotapermanente

class ListaRotasConcluidas : AppCompatActivity() {

    private lateinit var mView: View
    lateinit var mAdapter: AdapterListRotaConcluidas
    lateinit var pass: User

    init {
        pass = User()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_fragment_rota_concluidas)
        var rota = intent.extras.getString("rota")
        rotapermanente.layoutManager = LinearLayoutManager(applicationContext)
        this.voltar.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }
        Thread {
            Log.e("rota", rota)
            var all = AppDatabase.use(applicationContext).volumeDao().findaRotasBipador(Values.getCpf(), rota)
            Log.e("rotassize", all.size.toString())
            runOnUiThread {
                rotapermanente!!.adapter = AdapterListRotaConcluidas(
                    (all as ArrayList<Rota>?)!!,
                    applicationContext
                )
            }
        }.start()
        mAdapter = AdapterListRotaConcluidas(ArrayList(), applicationContext)
        aturotaPermanente.setOnRefreshListener {
            Log.e("atualizar", "atualiza")
            Thread {
                VolleyGet.AtualizarRotas(this, applicationContext)
                var all = AppDatabase.use(applicationContext).volumeDao().findallrotas()
                runOnUiThread {
                    mAdapter = AdapterListRotaConcluidas(
                        all as ArrayList<Rota>
                        , applicationContext
                    )
                    rotapermanente.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()
                    aturotaPermanente.isRefreshing = false
                }
            }.start()
        }

    }


}
