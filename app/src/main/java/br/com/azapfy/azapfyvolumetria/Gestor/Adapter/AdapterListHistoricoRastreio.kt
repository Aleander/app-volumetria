package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.app.Dialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_rastreio.view.*
import kotlinx.android.synthetic.main.popup_historico.*
import org.json.JSONObject


class AdapterListHistoricoRastreio(val codigos: MutableList<Codigo>, val context: Context) :
    RecyclerView.Adapter<ViewHolderHistoricoRastreio>() {
    var items = ArrayList<Codigo>(codigos)
    override fun onBindViewHolder(p0: ViewHolderHistoricoRastreio, p1: Int) {
        var aux = 1
        if (items.get(p1).cpfbipador != null && items.get(p1).cpfbipador != "") {
            p0.tipo?.text = "Roteirizado"
            p0.data?.text = items.get(p1).databipar
            aux = 1
        }
        if (items.get(p1).verificacao != null && items.get(p1).verificacao != "") {
            p0.image.setImageResource(R.drawable.boxopen)
            if (items.get(p1).rota_bip == JSONObject(items.get(p1).verificacao).getString("rota_veri").toString()) {
                p0.tipo?.text = "Conferido"
            } else {
                p0.tipo?.text = "Conferido Erro"
            }
            p0.data?.text = JSONObject(items.get(p1).verificacao).getString("data_veri")
            aux = 2
        }
        if (items.get(p1).reconferido != null && items.get(p1).reconferido != "") {
            p0.image.setImageResource(R.drawable.boxopen)
            if (items.get(p1).rota_bip == JSONObject(items.get(p1).reconferido).getString("rota_reco").toString()) {
                p0.tipo?.text = "Reconferido"
            } else {
                p0.tipo?.text = "Reconferido Erro"
            }
            p0.data?.text = JSONObject(items.get(p1).reconferido).getString("data_reco")
            aux = 3
        }
        p0.finali.setOnClickListener {
            if (aux == 1) {
                val dialog = Dialog(context)
                dialog.setContentView(R.layout.popup_historico)
                dialog.titlerota.setText(items.get(p1).rota_bip)
                dialog.titledata.setText(items.get(p1).databipar)
                dialog.titlecpf.setText(items.get(p1).cpfbipador)
                dialog.titlenome.setText(items.get(p1).nome_bipador)
                dialog.show()
            } else if (aux == 2) {
                val dialog = Dialog(context)
                var obj = JSONObject(items.get(p1).verificacao)
                dialog.setContentView(R.layout.popup_historico)
                dialog.titlerota.setText(obj.getString("rota_veri"))
                dialog.titledata.setText(obj.getString("data_veri"))
                dialog.titlecpf.setText(obj.getString("cpf_veri"))
                if (obj.has("nome_veri")) {
                    dialog.titlenome.setText(obj.getString("nome_veri"))
                }
                dialog.show()
            } else if (aux == 3) {
                val dialog = Dialog(context)
                var obj = JSONObject(items.get(p1).reconferido)
                dialog.setContentView(R.layout.popup_historico)
                dialog.titlerota.setText(obj.getString("rota_reco"))
                dialog.titledata.setText(obj.getString("data_reco"))
                dialog.titlecpf.setText(obj.getString("cpf_reco"))
                if (obj.has("nome_reco")) {
                    dialog.titlenome.setText(obj.getString("nome_reco"))
                }
                dialog.show()
            }
        }
    }

    fun filter(query: String) {
        items.clear()
        if (!query.isEmpty()) {
            for (x in codigos!!) {
                Log.e("codigostext", query)
                if (x.numero.contains(query)
                ) {
                    Log.e("x", items.toString())
                    items.add(x)
                }
            }
        } else {
            items.addAll(codigos!!)
        }
        Log.e("x", items.toString())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderHistoricoRastreio {
        return ViewHolderHistoricoRastreio(
            LayoutInflater.from(context).inflate(
                R.layout.item_rastreio,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }
}


class ViewHolderHistoricoRastreio(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val data = itemView.data
    val tipo = itemView.tipo
    val relative = itemView.relative
    val finali = itemView.finali
    val image = itemView.image

    init {

    }
}



