package br.com.azapfy.azapfyvolumetria.Gestor.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListRotaAdm
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_fragment_rotaadm.*


class FragmentRotasAdm : Fragment() {
    lateinit var mAdapter: AdapterListRotaAdm
    lateinit var pass: User
    init {
        pass = User()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_fragment_rotaadm, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rv_rotalist.layoutManager = LinearLayoutManager(context)
        Thread {
            var all = AppDatabase.use(context).volumeDao().findallrotas()
            activity?.runOnUiThread {
                mAdapter = AdapterListRotaAdm(
                    all!!,
                    requireContext()
                )
                rv_rotalist.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListRotaAdm(ArrayList(), requireContext())
        atuRotaAdm.setOnRefreshListener {
            Log.e("atualizar", "atualiza")
            Thread {
                VolleyGet.AtualizarRotas(requireActivity(), requireContext())
                var all = AppDatabase.use(context).volumeDao().findallrotas()
                activity!!.runOnUiThread {
                    mAdapter = AdapterListRotaAdm(
                        all as ArrayList<Rota>
                        , requireContext()
                    )
                    rv_rotalist.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()
                    atuRotaAdm.isRefreshing = false
                }
            }.start()
        }
        rotatext.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mAdapter!!.filter(s.toString())
            }
        })

    }


    fun getAdapter(): AdapterListRotaAdm {
        return mAdapter
    }


}