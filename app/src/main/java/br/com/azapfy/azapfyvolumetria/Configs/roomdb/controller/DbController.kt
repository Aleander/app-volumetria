package br.com.azapfy.azapfyvolumetria.Configs.roomdb.controller

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.telephony.TelephonyManager
import android.util.Log
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo


object DbController {
    fun insertCodigo(
        context: Context,
        numero: String,
        valor: String,
        descricao: String,
        tipo: String,
        id: String,
        databip: String,
        cpf: String,
        cnpj: String,
        pendente: Boolean,
        situacao: String

    ): Boolean {
        try {
            if (tipo == "romaneio") {
                val newCodigo = Codigo()
                newCodigo.id
                newCodigo.numero = numero
                newCodigo.romaneio_bip = valor
                newCodigo.descricao = descricao
                newCodigo._id = id
                newCodigo.databipar = databip
                newCodigo.cpfbipador = cpf
                newCodigo.cnpj = cnpj
                newCodigo.pendente = pendente
                newCodigo.situacao = situacao
                AppDatabase.use(context).codigoDao().insert(newCodigo)
            } else if (tipo == "rota") {
                val newCodigo = Codigo()
                newCodigo.id
                newCodigo.numero = numero
                newCodigo.rota_bip = valor
                newCodigo.descricao = descricao
                newCodigo._id = id
                newCodigo.databipar = databip
                newCodigo.cpfbipador = cpf
                newCodigo.cnpj = cnpj
                newCodigo.pendente = pendente
                newCodigo.situacao = situacao
                AppDatabase.use(context).codigoDao().insert(newCodigo)
            }

            return true
        } catch (e: java.lang.Exception) {
            Log.e("teste", e.toString())
            return false
        }
    }

    fun getIMEI(activity: Activity): String {
        val telephonyManager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE)
            == PackageManager.PERMISSION_GRANTED
        ) {
            telephonyManager.deviceId
        } else {
            "error"
        }
    }

    fun insertCodigoVeri(
        context: Context,
        numero: String,
        nome: String,
        descricao: String,
        tipo: String,
        id: String,
        databip: String,
        cpf: String,
        cnpj: String,
        pendente: Boolean,
        situacao: String
    ): Boolean {
        try {
            if (tipo == "romaneio") {
                val newCodigo = Codigo()

                newCodigo.id
                newCodigo.numero = numero
                newCodigo.descricaoveri = descricao
                newCodigo._id = id
                newCodigo.dataveri = databip
                newCodigo.cpfveri = cpf
                newCodigo.cnpj = cnpj
                newCodigo.pendente = pendente
                newCodigo.situacao = situacao
                AppDatabase.use(context).codigoDao().insert(newCodigo)
            } else if (tipo == "rota") {

                val newCodigo = Codigo()
                newCodigo.id
                newCodigo.numero = numero
                newCodigo.rota_veri = nome
                newCodigo.dataveri = databip
                newCodigo.cpfveri = cpf
                newCodigo.descricaoveri = descricao
                newCodigo._id = id
                newCodigo.cnpj = cnpj
                newCodigo.pendente = pendente
                newCodigo.situacao = situacao
                AppDatabase.use(context).codigoDao().insert(newCodigo)
            }

            return true
        } catch (e: java.lang.Exception) {
            Log.e("teste", e.toString())
            return false
        }
    }


}
