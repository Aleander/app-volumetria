package br.com.azapfy.azapfyvolumetria.Gestor.Activity

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.R
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import kotlinx.android.synthetic.main.activity_dashboard.*


class Dashboard : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        val NoOfEmp = ArrayList<BarEntry>()
        var total = 0
        Thread {
            var rota = AppDatabase.use(applicationContext).volumeDao().findallrotas()
            val VORDIPLOM_COLORS = arrayListOf<Int>()

            var nome = ArrayList<String>()
            var num = 2
            var aux = 0
            VORDIPLOM_COLORS.add(Color.rgb(255, 80, 0))
            for (x in rota!!) {
                aux = aux + 1
                NoOfEmp.add(BarEntry(aux.toFloat(), x.quantidade_bipada_total.toFloat()))
                nome.add(x.rota)
                total = total + x.quantidade_bipada_total.toString().toInt()
                val color = Color.rgb(255, 80, 0)
                val x = color * num
                num = num + 5
                VORDIPLOM_COLORS.add(x)
            }
            runOnUiThread {
                val barWidth: Float
                val barSpace: Float
                val groupSpace: Float
                val groupCount = 12

                barWidth = 1.00f
                barSpace = 0.07f
                groupSpace = 0.56f
                var barDataSet1: BarDataSet
                barDataSet1 = BarDataSet(NoOfEmp, "")
                barDataSet1.setColors(VORDIPLOM_COLORS)
                barDataSet1.setDrawIcons(false)
                barDataSet1.setDrawValues(false)
                var barData = BarData(barDataSet1)
                barChartView.description.isEnabled = false
                barChartView.groupBars(0f, groupSpace, barSpace)
                barChartView.description.textSize = 0f
                barData.setValueFormatter(LargeValueFormatter())
                barChartView.setData(barData)
                barChartView.getBarData().setBarWidth(barWidth)
                barChartView.getXAxis().setAxisMinimum(0f)
                barChartView.getXAxis().setAxisMaximum(7f)
                barChartView.getData().setHighlightEnabled(false)
                barChartView.invalidate()
                var legend = barChartView.legend
                legend.setDrawInside(false)

                var legenedEntries = arrayListOf<LegendEntry>()
                legend.setCustom(legenedEntries)

                legend.setYOffset(2f)
                legend.setXOffset(2f)
                legend.setYEntrySpace(0f)
                legend.setTextSize(5f)

                val xAxis = barChartView.getXAxis()
                xAxis.setGranularity(1f)
                xAxis.setGranularityEnabled(true)
                xAxis.setCenterAxisLabels(true)
                xAxis.setDrawGridLines(false)
                xAxis.textSize = 9f

                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM)
                xAxis.setValueFormatter(IndexAxisValueFormatter(nome))

                xAxis.setLabelCount(12)
                xAxis.mAxisMaximum = 12f
                xAxis.setCenterAxisLabels(true)
                xAxis.setAvoidFirstLastClipping(true)
                xAxis.spaceMin = 4f
                xAxis.spaceMax = 4f

                barChartView.setVisibleXRangeMaximum(12f)
                barChartView.setVisibleXRangeMinimum(12f)
                barChartView.setDragEnabled(true)

                //Y-axis
                barChartView.getAxisRight().setEnabled(false)
                barChartView.setScaleEnabled(true)

                val leftAxis = barChartView.getAxisLeft()
                leftAxis.setValueFormatter(LargeValueFormatter())
                leftAxis.setDrawGridLines(false)
                leftAxis.setSpaceTop(1f)
                leftAxis.setAxisMinimum(0f)


                barChartView.data = barData
                barChartView.setVisibleXRange(1f, 12f)
            }
        }.start()

    }

}

