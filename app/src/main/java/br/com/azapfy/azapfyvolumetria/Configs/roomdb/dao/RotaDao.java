package br.com.azapfy.azapfyvolumetria.Configs.roomdb.dao;

import android.arch.persistence.room.*;
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota;

import java.util.List;


@Dao
public interface RotaDao {


    @Insert
    void insert(Rota rota);

    @Query("SELECT *  FROM Rota")
    List<Rota> findallrotas();


    @Query("SELECT *  FROM Rota WHERE cpf = :cpf  AND rota = :rota")
    List<Rota> findaRotasBipador(String cpf , String rota);

    @Query("SELECT id FROM Rota WHERE data = :horario ")
    Integer findReceivedConfirmationQueue(String horario);

    @Query("SELECT  rota FROM Rota  WHERE _id = :id")
    String findnome(String id);

    @Query("SELECT  rota FROM Rota  WHERE rota = :id")
    String findveriRotas(String id);

    @Query("SELECT  _id FROM Rota  WHERE _id = :id")
    String findveriRotasId(String id);

    @Query("DELETE  FROM  Rota  WHERE _id = :x ")
    void deleterota(String x);

    @Query("UPDATE Rota SET valida = :valor WHERE _id =:horario")
    void validar(Integer valor, String horario);

    @Query("SELECT * FROM Rota WHERE situacao = :situacao ")
    List<Rota> getRotaSituacao(String situacao);

    @Query("SELECT * FROM Rota WHERE rota = :rota ")
    List<Rota> findinfo(String rota);

    @Query("UPDATE Rota SET  quantidade_bipada_total = :quant WHERE rota = :rota")
    void updateRota(String quant, String rota);

    @Update
    void update(Rota rota);

    @Delete
    void delete(Rota rota);
}
