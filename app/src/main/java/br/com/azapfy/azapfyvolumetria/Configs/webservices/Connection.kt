package br.com.azapfy.azapfymotoristas.utils.webservices

import android.app.Activity
import android.content.Context
import org.json.JSONObject
import java.net.MalformedURLException
import java.net.URL

class Connection @Throws(MalformedURLException::class) constructor(
    address: String,
    data: JSONObject,
    context: Activity
) {
    var url: URL = URL(address)
    var `object`: JSONObject = data
    var ctx: Context = context
    var activity: Activity = context
}
