package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.SubRotasGestor
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_codigo.view.*
import kotlinx.android.synthetic.main.item_rotaadm.view.*

class AdapterListRotaAdm(
    val items: MutableList<Rota>,
    val context: Context?

) : RecyclerView.Adapter<ViewHoldeRotaAdm>() {
    var mylist: ArrayList<String> = ArrayList()
    var rotas = ArrayList<Rota>(items)
    override fun onBindViewHolder(p0: ViewHoldeRotaAdm, p1: Int) {
        p0.rota?.text = rotas.get(p1).rota
        p0.data?.text = rotas.get(p1).data.replace("-", "/")
        p0.quantidade?.text = rotas.get(p1).quantidade_bipada_total
        p0.clientevalue?.text = rotas.get(p1).embarcador
        if (rotas.get(p1)._id == null) {
            p0._id = "0"
        } else {
            p0._id = rotas.get(p1)._id
        }
        if (rotas.get(p1).situacao == "cadastrado") {
            p0.porce.visibility = View.GONE
            p0.rotatext.visibility = View.GONE
        } else {
            p0.porce?.progress = rotas.get(p1).porcentagem
            p0.rotatext?.text = rotas.get(p1).quantidade.toString() + "/" + rotas.get(p1).limite
        }

        p0.itemView.setOnLongClickListener {
            if (mylist.contains(p0._id)) {
                p0.itemView.setBackgroundResource(R.drawable.button_2)
                mylist.remove(p0._id)
                Log.e("List", "${Values.arraySelection.size}" + "quant" + Values.arraySelection.toString())

            } else {
                p0.itemView.setBackgroundResource(R.drawable.button_vinculation)
                mylist.add(p0._id)

            }
            true
        }


    }

    fun filter(query: String) {
        rotas.clear()
        if (!query.isEmpty()) {
            for (rota in items!!) {
                if ((rota).rota!!.toUpperCase().contains(query.toUpperCase())) {
                    Log.e("x", rotas.toString())
                    rotas.add(rota)
                }
            }
        } else {
            rotas.addAll(items!!)
        }
        Log.e("x", rotas.toString())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldeRotaAdm {
        return ViewHoldeRotaAdm(
            LayoutInflater.from(context).inflate(
                R.layout.item_rotaadm,
                p0,
                false
            )
        )
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return rotas.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}

class ViewHoldeRotaAdm(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {

    val rota = itemView.rotaadm
    val data = itemView.dataadmrota
    val quantidade = itemView.quantidadeadmrota
    val porce = itemView.progrota
    val rotatext = itemView.rotatext
    val clientevalue = itemView.clientevalue

    val opc = itemView.opc
    var _id = ""

    init {
        itemView.setOnClickListener {
            Log.e("rota", _id)
            Values.setPassTipo("rota")
            Values.setPassId(_id)
            Values.setPassRota(rota.text.toString())
            val intent = Intent(itemView.getContext(), SubRotasGestor::class.java)
            itemView.getContext().startActivity(intent)
        }


    }

    override fun onLongClick(v: View?): Boolean {

        return false
    }

    override fun onClick(view: View) {

    }

    companion object {
        private val numero = ArrayList<Int>()
    }


}