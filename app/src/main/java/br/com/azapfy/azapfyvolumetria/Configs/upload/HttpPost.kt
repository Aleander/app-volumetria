package br.com.azapfy.azapfyvolumetria.Configs.upload

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.SubRota
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.Configs.utils.SimpleDialog
import br.com.azapfy.azapfyvolumetria.MainActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets

class HttpPost : AsyncTask<Connection, String, String>() {
    private var mProgressDialog: ProgressDialog? = null
    private var urlId: Int = 0
    private fun defineUrlId(url: URL, mConnection: Connection) {
        urlId = ServicesUrl.getServiceCode(url)
        Log.e("loginurl", url.toString())
        if (urlId == ServicesUrl.VALIDATE) {
            mConnection.activity.runOnUiThread {
                Log.e("conect3", "log")
            }
        }
    }

    fun postWithVolley(connection: Connection) {
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {
                Thread {
                    LogController.e(
                        "WebResponse",
                        "Value URL ID: $urlId | Connection URL: ${connection.url} | Response from service: $it"
                    )
                    when (urlId) {
                        ServicesUrl.VALIDATE -> validateLogin(it.toString(), connection)
                    }
                    connection.activity.runOnUiThread {
                        Toast.makeText(connection.ctx, "CRIADO COM SUCESSO.", Toast.LENGTH_SHORT).show()
                    }
                    dispatch(connection)
                    //serverResp.setText("String Response : " + response.toString())
                }.start()
            }, Response.ErrorListener {
                Thread {
                    LogController.e(TAG, "Font: ${urlId} | Error #2: $it")
                    if (urlId != ServicesUrl.VALIDATE && !it.toString().contains("java.io.FileNotFoundException")) {

                    } else {
                        if (urlId == ServicesUrl.VALIDATE) SimpleDialog.show(
                            connection.activity,
                            "CPF ou senha incorreta."
                        )
                    }
                    dispatch(connection)
                    //serverResp.setText("Error getting response")
                }.start()
            })
        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)
    }

    fun post(connection: Connection, context: Activity?) {
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {
                Thread {
                    Log.e("atualizarsuce", it.toString())
                    dispatch(connection)
                    VolleyGet.request(it, context)
                }.start()
            }, Response.ErrorListener {
                Thread {
                    LogController.e("atualizarerro", "Font: ${urlId} | Error #2: $it")
                    dispatch(connection)
                }.start()
            })
        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)


    }


    fun postPesquisa(connection: Connection, context: Activity?): Codigo {
        defineUrlId(connection.url, connection)
        val novaCodigo = Codigo()
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {

                if (it.has("valor")) {
                    val result = it.getJSONObject("valor")
                    for (i in 0 until result.length()) {
                        if (result.has("_id")) {
                            novaCodigo._id = result.get("_id").toString()
                        } else {
                            novaCodigo._id = ""
                        }
                        if (result.has("cnpj_empresa")) {
                            novaCodigo.cnpj = result.get("cnpj_empresa").toString()
                        } else {
                            novaCodigo.cnpj = ""
                        }
                        if (result.has("situacao")) {
                            novaCodigo.situacao = result.get("situacao").toString()
                        } else {
                            novaCodigo.situacao = ""
                        }
                        if (result.has("erro")) {
                            novaCodigo.erro = result.getBoolean("erro")
                        } else {
                            novaCodigo.erro = false
                        }

                        if (result.has("bipado")) {
                            var bipado = result.getJSONObject("bipado")
                            Log.e("tembipado", bipado.toString())
                            if (bipado.has("msg_bipado")) {
                                novaCodigo.descricao = bipado.getString("msg_bipado")
                            } else {
                                novaCodigo.descricao = ""
                            }

                            if (bipado.has("cpf_bipador")) {
                                novaCodigo.cpfbipador = bipado.getString("cpf_bipador")
                            } else {
                                novaCodigo.cpfbipador = ""
                            }

                            if (bipado.has("data_bip")) {
                                novaCodigo.databipar = bipado.getString("data_bip")
                            } else {
                                novaCodigo.databipar = ""
                            }

                            if (bipado.has("subrota_bip")) {
                                novaCodigo.subrota_bip = bipado.getString("subrota_bip")
                            } else {
                                novaCodigo.subrota_bip = ""
                            }

                            if (bipado.has("rota_bip")) {
                                novaCodigo.rota_bip = bipado.getString("rota_bip")
                            } else {
                                novaCodigo.rota_bip = ""
                            }
                        }
                        if (result.has("verificador")) {
                            var veri = result.getJSONObject("verificador")
                            Log.e("temveri", veri.toString())
                            if (veri.has("msg_veri")) {
                                novaCodigo.descricaoveri = veri.getString("msg_veri")
                            } else {
                                novaCodigo.descricaoveri = ""
                            }
                            if (veri.has("cpf_veri")) {
                                novaCodigo.cpfveri = veri.getString("cpf_veri")
                            } else {
                                novaCodigo.cpfveri = ""
                            }
                            if (veri.has("data_veri")) {
                                novaCodigo.dataveri = veri.getString("data_veri")
                            } else {
                                novaCodigo.dataveri = ""
                            }
                            if (veri.has("rota_veri")) {
                                novaCodigo.rota_veri = veri.getString("rota_veri")
                            } else {
                                novaCodigo.rota_veri = ""
                            }
                        }
                        if (result.has("codigo") && result.has("verificador") && result.has("bipado")
                        ) {
                            if (novaCodigo.rota_bip.toString() == novaCodigo.rota_veri.toString()) {
                                novaCodigo.numero = result.get("codigo").toString()
                                novaCodigo.erro = false
                            } else {
                                novaCodigo.numero = result.get("codigo").toString()
                                novaCodigo.erro = true
                            }
                        } else if (result.has("codigo") && result.has("verificador")){
                            novaCodigo.numero = result.get("codigo").toString()
                            novaCodigo.erro = true
                        } else {
                            novaCodigo.numero = result.get("codigo").toString()
                            novaCodigo.erro = true
                        }
                    }

                }

                Log.e("xxxxxxxxxxxxxxx ", novaCodigo.numero)
                dispatch(connection)

            }, Response.ErrorListener {
                Thread {
                    LogController.e("atualizarerro", "Font: ${urlId} | Error #2: $it")
                    dispatch(connection)
                }.start()
            })

        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)
        Log.e("xxxxxxxxxxxxxxx ", novaCodigo.numero)
        return novaCodigo
    }


    fun postSubRotas(connection: Connection) {
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {
                Thread {
                    connection.activity.runOnUiThread {
                        Toast.makeText(connection.ctx, "CRIADO COM SUCESSO.", Toast.LENGTH_SHORT).show()
                    }
                    Log.e("ATUALIZASUB", it.toString())
                    dispatch(connection)
                }.start()
            }, Response.ErrorListener {
                Thread {
                    LogController.e("ATUALIZASUBERRO", "Font: ${urlId} | Error #2: $it")
                    dispatch(connection)
                }.start()
            })
        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)


    }

    fun retornaCodigos(connection: Connection, act: Activity?) {
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {
                VolleyGet.requestCodigos(it, act)
                dispatch(connection)
            }, Response.ErrorListener {
                Thread {
                    LogController.e(
                        "atualizarerroVALIDAR", "Font: ${urlId} | Error #2: $it"
                    )
                    dispatch(connection)
                }.start()
            })

        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)


    }

    fun postHist(connection: Connection) {
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {
                Log.e("atualizarenvio", it.toString())
                dispatch(connection)
            }, Response.ErrorListener {
                Thread {
                    LogController.e(
                        "atualizarerroVALIDAR", "Font: ${urlId} | Error #2: $it"
                    )
                    dispatch(connection)
                }.start()
            })

        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)


    }

    fun postEnviarCodigos(connection: Connection) {
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {
                Thread {
                    Log.e("atualizarenvio", it.toString())
                    if ("true" == it.get("resultado").toString()) {
                        val SubRota = SubRota()
                        if (it.has("id_rota")) {
                            SubRota.id_rota = it.get("id_rota").toString()
                            if (it.has("data_create")) {
                                SubRota.id_rota = it.get("data_create").toString()
                            }
                            AppDatabase.use(connection.activity).subRotaDao().insert(SubRota)
                        }
                    }
                    dispatch(connection)
                }.start()
            }, Response.ErrorListener {
                Thread {
                    LogController.e(
                        "atualizarerro", "Font: ${urlId} | Error #2: $it"
                    )
                    dispatch(connection)
                }.start()
            })

        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)


    }


    fun postAdvanced(connection: Connection): String {
        var x = null
        defineUrlId(connection.url, connection)
        val requestQueue = VolleyGet.getRequestQueue(connection.activity)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, connection.url.toString(), connection.`object`,
            Response.Listener<JSONObject> {

                Log.e("atualizar", it.toString())
                dispatch(connection)
                x == it.toString()
            }, Response.ErrorListener {
                Thread {
                    LogController.e("atualizarerro", "Font: ${urlId} | Error #2: $it")
                    dispatch(connection)
                }.start()
            })
        jsonObjectRequest.tag = urlId
        requestQueue?.add(jsonObjectRequest)

        return x.toString()
    }

    override fun doInBackground(vararg connections: Connection): String? {
        if (connections.isEmpty()) {
            return "Error"
        }
        val mConnection = connections[0]
        defineUrlId(mConnection.url, mConnection)
        try {
            val connection = mConnection.url
                .openConnection() as HttpURLConnection
            LogController.e(TAG, mConnection.`object`.toString())
            connection.setRequestProperty("Accept", "application/json")
            connection.setRequestProperty("Content-type", "application/json")

            connection.requestMethod = "POST"
            connection.doInput = true
            connection.doOutput = true

            val outputStream = connection.outputStream
            val writer = BufferedWriter(OutputStreamWriter(outputStream, StandardCharsets.UTF_8))

            writer.write(mConnection.`object`.toString())

            writer.flush()
            writer.close()
            outputStream.close()

            connection.connect()


            val stream = BufferedInputStream(connection.inputStream)
            val responseStreamReader = BufferedReader(InputStreamReader(stream))
            var line: String? = ""
            val stringBuilder = StringBuilder()
            do {
                line = responseStreamReader.readLine()
                if (line != null) {
                    stringBuilder.append(line)
                }
            } while (line != null)
            responseStreamReader.close()

            val response = stringBuilder.toString()

            LogController.e(
                "WebResponse",
                "Value URL ID: $urlId | Connection URL: ${mConnection.url} | Response from service: $response"
            )

            when (urlId) {
                ServicesUrl.VALIDATE -> validateLogin(response, mConnection)
            }
            dispatch(mConnection)
            return response
        } catch (e: Exception) {
            LogController.e(TAG, "Font: ${urlId} | Error #2: $e")

            if (urlId != ServicesUrl.VALIDATE && !e.toString().contains("java.io.FileNotFoundException")) {

            } else {
                if (urlId == ServicesUrl.VALIDATE) SimpleDialog.show(mConnection.activity, "CPF ou senha incorreta.")
            }
            dispatch(mConnection)
        }

        return null
    }

    @Throws(JSONException::class)
    private fun validateLogin(response: String, mConnection: Connection) {

        if (response != null) {
            LogController.e(TAG, response)
            val hasData = true
            if (response.contains("Unauthorised")) {
                mProgressDialog?.dismiss()
                Log.e("testecone", "CPF ou senha incorreta.")
                return
            }
            /*when (response) {
                "invalid" -> SimpleDialog.show(mConnection.activity, "O CPF digitado" + " é inválido.")
                "wrong_password" -> SimpleDialog.show(mConnection.activity, "O senha " +
                        "digitada está " +
                        "incorreta.")
                "error" -> SimpleDialog.show(mConnection.activity, "Ocorreu um " +
                        "erro desconhecido" +
                        ". Tente novamente.")
                else -> hasData = true
            }*/
            if (hasData) {

                val user = JSONObject(response).getJSONObject("user").getJSONObject("data")
                val emp = JSONObject(response)
                Log.e("emp", emp.toString())
                var u: User? = AppDatabase.use(mConnection.ctx).userDao().findByCpf(user.getString("cpf"))
                if (u == null) {
                    u = User()
                    u.tipo = user.getString("grupo_usuario")
                    if (user.has("cnpj_empresa")) {
                        u.cnpj = user.getString("cnpj_empresa")
                    }
                    u.cpf = user.getString("cpf")
                    u.name = user.getString("name")
                    u.auth = true
                    if (user.has("telefone")) {
                        u.telefone = user.getString("telefone")
                    } else {
                        u.telefone = user.getString("Nao cadastrado")
                    }
                    //u.base = user.getString("base")
                    u.grupo_emp = user.getString("grupo_emp")
                    if (emp.has("tipo_empresa")) {
                        u.tipo_emp = emp.getString("tipo_empresa")
                    } else {
                        u.tipo_emp = "dist"
                    }
                    val config = JSONObject()
                    config.put("auto_delete", true)
                    config.put("cellular_data", true)
                    config.put("tipo_cam", true)
                    config.put("teclado", false)
                    val url =
                        if ("http://homologacao.azapfy.com.br/api/android/" == br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_LOCAL) {
                            br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_HOMOLOG
                        } else {
                            br.com.azapfy.azapfyvolumetria.Configs.constants.Constants.WEBSERVICE_LOCAL
                        }
                    config.put("webservice", url)
                    if (user.has("grupo_emp")) {
                        config.put("grupo_emp", user.getString("grupo_emp"))
                    }
                    u.setConfig(config)
                    AppDatabase.use(mConnection.ctx).userDao().insert(u)
                } else {
                    u.auth = true
                    AppDatabase.use(mConnection.ctx).userDao().update(u)
                }
                Values.setUser(u.tipo)
                Values.setCnpj(u.cnpj)
                Values.setCpf(u.cpf)
                val i = Intent(mConnection.activity, MainActivity::class.java)

                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                mConnection.activity.startActivity(i)
            }
        }
        dispatch(mConnection)
    }

    private fun dispatch(mConnection: Connection) {
        if (urlId == ServicesUrl.VALIDATE) {
            mConnection.activity.runOnUiThread { if (mProgressDialog != null) mProgressDialog!!.dismiss() }
        }
    }

    public override fun onPostExecute(s: String?) {
        if (s != null && s == "success") {

        }
    }

    companion object {

        private val TAG = "Class$1"
    }
}
