package br.com.azapfy.azapfyvolumetria.bipador.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Romaneio
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Activity.Camera
import kotlinx.android.synthetic.main.item_romaneio_contador.view.*


class AdapterListRomaneio(
    val items: ArrayList<Romaneio>,
    val context: Context?
) : RecyclerView.Adapter<ViewHoldeRomaneioContadorPendente>() {

    var nRomaneio = ArrayList<Romaneio>(items)
    override fun onBindViewHolder(p0: ViewHoldeRomaneioContadorPendente, p1: Int) {
        p0.data?.text = nRomaneio.get(p1).data.replace("-", "/").toString()
        p0.numero?.text = nRomaneio.get(p1).nRomaneio.toString()
        p0.id = nRomaneio.get(p1)._id


    }

    fun filter(query: String) {
        nRomaneio.clear()
        if (!query.isEmpty()) {
            for (romaneio in items!!) {
                if ((romaneio).embarcador!!.toUpperCase().contains(query.toUpperCase())
                    || (romaneio).tomador!!.toUpperCase().contains(query.toUpperCase())
                    || (romaneio).nRomaneio!!.toUpperCase().contains(query.toUpperCase())
                ) {
                    Log.e("x", nRomaneio.toString())
                    nRomaneio.add(romaneio)
                }
            }
        } else {
            nRomaneio.addAll(items!!)
        }
        Log.e("x", nRomaneio.toString())
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldeRomaneioContadorPendente {
        return ViewHoldeRomaneioContadorPendente(
            LayoutInflater.from(context).inflate(
                R.layout.item_romaneio_contador,
                p0,
                false
            )
        )
    }


    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return nRomaneio.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view

}


class ViewHoldeRomaneioContadorPendente(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {
    override fun onLongClick(v: View?): Boolean {
        return false
    }

    val numero = itemView.numero
    val data = itemView.DATAN
    var id = ""
    lateinit var pass: User
    lateinit var passC: Codigo

    init {
        itemView.setOnClickListener {
            pass = User()
            passC = Codigo()
            passC.setPass(1)
            Values.setPassTipo("romaneio")
            Values.setPassromaneio(numero.text.toString())
            Values.setPassId(id)
            val intent = Intent(itemView.getContext(), Camera::class.java)
            itemView.getContext().startActivity(intent)
        }
    }
    override fun onClick(view: View) {

    }




}