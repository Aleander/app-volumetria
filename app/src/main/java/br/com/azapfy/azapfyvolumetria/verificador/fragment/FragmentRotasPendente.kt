package br.com.azapfy.azapfyvolumetria.verificador.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.verificador.Adapter.AdapterListRotaPendente
import kotlinx.android.synthetic.main.activity_fragment_rota_pendente.*

class FragmentRotasPendente : Fragment() {

    lateinit var mAdapter: AdapterListRotaPendente

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_fragment_rota_pendente, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        rotapendente.layoutManager = LinearLayoutManager(context)

        Thread {
            var nRota = ArrayList<Rota>()
            Log.e("Tipo user", Values.getUser())
            if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
                nRota = AppDatabase.use(context).volumeDao().getRotaSituacao("cadastrado") as ArrayList<Rota>
                Log.e("rotasss", nRota.toString())
            } else {
                nRota = AppDatabase.use(context).volumeDao().getRotaSituacao("bipado") as ArrayList<Rota>
            }

            Log.e("rotas", nRota.toString())
            activity?.runOnUiThread {
                mAdapter = AdapterListRotaPendente(
                    nRota,
                    requireContext()
                )
                rotapendente?.adapter = mAdapter

            }
        }.start()
        mAdapter = AdapterListRotaPendente(
            ArrayList(),
            requireContext()
        )
        searchViewRotaPendente.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mAdapter!!.filter(s.toString())
            }
        }
        )
        atuRotaPendente.setOnRefreshListener {
            Log.e("atualizar", "atualiza")
            Thread {
                VolleyGet.AtualizarRotas(requireActivity(), requireContext())
                /*     val json = JSONObject()
                     json.put("id_rota", "082954409")
                     json.put("rota", "xxt")
                     val connection = Connection(
                         ServicesUrl.getServiceURL(requireContext(), ServicesUrl.SUBROTARETURN), json,
                         requireActivity()
                     )
                     HttpPost().postSubRotas(connection, requireActivity())*/

                val nRota = AppDatabase.use(context).volumeDao().getRotaSituacao("cadastrado")
                Log.e("rotasss", nRota.toString())
                activity!!.runOnUiThread {
                    mAdapter = AdapterListRotaPendente(
                        nRota as ArrayList<Rota>
                        , requireContext()
                    )
                    rotapendente.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()
                    atuRotaPendente.isRefreshing = false
                }
            }.start()
        }
    }

    fun getAdapter(): AdapterListRotaPendente {
        return mAdapter
    }

}
