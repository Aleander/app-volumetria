package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.HistoricoRastreio
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_resu_historico.view.*
import org.json.JSONObject


class AdapterListHistoricoResu(val codigos: MutableList<Codigo>, val context: Context) :
    RecyclerView.Adapter<ViewHolderHistoricoResu>() {
    var items = ArrayList<Codigo>(codigos)
    override fun onBindViewHolder(p0: ViewHolderHistoricoResu, p1: Int) {
        var validacao = 0

        if (items.get(p1).cpfbipador != null && items.get(p1).cpfbipador != "") {
            p0.rota?.text = items.get(p1).rota_bip
            p0.numero?.text = items.get(p1).numero
            p0.dataHist?.text = items.get(p1).databipar.replace("-", "/")
            validacao = 1

        }
        if (items.get(p1).verificacao != null && items.get(p1).verificacao != "") {
            p0.rota?.text = JSONObject(items.get(p1).verificacao).getString("rota_veri")
            p0.numero?.text = items.get(p1).numero
            p0.dataHist?.text = JSONObject(items.get(p1).verificacao).getString("data_veri").replace("-", "/")
            validacao = 2
        }
        if (items.get(p1).reconferido != null && items.get(p1).reconferido != "") {
            p0.rota?.text = JSONObject(items.get(p1).reconferido).getString("rota_reco")
            p0.numero?.text = items.get(p1).numero
            p0.dataHist?.text = JSONObject(items.get(p1).reconferido).getString("data_reco").replace("-", "/")
            validacao = 3
        }

        p0.opc.setOnClickListener {
            val v = p0.opc
            val location = IntArray(2)
            v.getLocationOnScreen(location)
            //creating a popup menu
            val popup = PopupMenu(context, v)
            //inflating menu from xml resource
            //adding click listener
            popup.inflate(br.com.azapfy.azapfyvolumetria.R.menu.menu_historico)
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    when (item.itemId) {
                        br.com.azapfy.azapfyvolumetria.R.id.wpp -> {
                            if (validacao == 1) {
                                val openURL = Intent(android.content.Intent.ACTION_VIEW)
                                openURL.data = Uri.parse(
                                    "https://api.whatsapp.com/send?phone=55${items.get(p1).telefone_bipador}text&text=O volume: ${items.get(
                                        p1
                                    ).numero}" +
                                            "%20pertence a rota ${items.get(p1).rota_bip}.%20 Gentileza envia-lo para a matriz. %20"
                                )
                                context.startActivity(openURL)
                            } else if (validacao == 2) {
                                val openURL = Intent(android.content.Intent.ACTION_VIEW)
                                openURL.data = Uri.parse(
                                    "https://api.whatsapp.com/send?phone=55${JSONObject(items.get(p1).verificacao).getString(
                                        "telefone_veri"
                                    )}text&text=O volume:${items.get(p1).numero}" +
                                            "%20pertence a rota ${JSONObject(items.get(p1).verificacao).getString("rota_veri")}.%20 Gentileza envia-lo para a matriz. %20"
                                )
                                context.startActivity(openURL)
                            } else if (validacao == 3) {
                                val openURL = Intent(android.content.Intent.ACTION_VIEW)
                                openURL.data = Uri.parse(
                                    "https://api.whatsapp.com/send?phone=55${JSONObject(items.get(p1).reconferido).getString(
                                        "telefone_reco"
                                    )}text&text=O volume:${items.get(p1).numero}" +
                                            "%20pertence a rota CURVELO.%20 Gentileza envia-lo para a matriz. %20"
                                )
                                context.startActivity(openURL)
                            }
                        }
                        br.com.azapfy.azapfyvolumetria.R.id.ligar -> {
                            if (validacao == 1) {

                                val uri = Uri.parse("tel:${items.get(p1).telefone_bipador}")
                                val intent = Intent(Intent.ACTION_DIAL, uri)
                                context.startActivity(intent)
                            } else if (validacao == 2) {

                                val uri =
                                    Uri.parse("tel:${JSONObject(items.get(p1).verificacao).getString("telefone_veri")}")
                                val intent = Intent(Intent.ACTION_DIAL, uri)
                                context.startActivity(intent)
                            } else if (validacao == 3) {

                                val uri =
                                    Uri.parse("tel:${JSONObject(items.get(p1).reconferido).getString("telefone_reco")}")
                                val intent = Intent(Intent.ACTION_DIAL, uri)
                                context.startActivity(intent)
                            }

                        }

                        br.com.azapfy.azapfyvolumetria.R.id.aprovar -> {
                            if (validacao == 2) {
                                Thread {
                                    AppDatabase.use(context).codigoDao()
                                        .updatecodigosAprovadoVeri(true, items.get(p1).numero)
                                }.start()
                                VolleyGet.aprovarCodigos(Activity(), context, Activity(), items.get(p1).numero)
                                p0.itemView?.visibility = View.GONE
                                items.removeAt(p0.adapterPosition);
                                notifyItemRemoved(p0.adapterPosition);
                                notifyItemRangeChanged(p0.adapterPosition, items.size);
                            } else {
                                Thread {
                                    AppDatabase.use(context).codigoDao().updatecodigosAprovadoBipa(
                                        true, items.get(p1).numero
                                    )
                                }.start()
                                VolleyGet.aprovarCodigos(Activity(), context, Activity(), items.get(p1).numero)
                                p0.itemView?.visibility = View.GONE
                                items.removeAt(p0.adapterPosition);
                                notifyItemRemoved(p0.adapterPosition);
                                notifyItemRangeChanged(p0.adapterPosition, items.size);

                            }
                        }

                    }//handle menu1 click
                    //handle menu2 click
                    //handle menu3 click
                    return false
                }
            })
            //displaying the popup
            popup.show()
        }
    }

    fun filter(query: String) {
        items.clear()
        if (!query.isEmpty()) {
            for (x in codigos) {
                Log.e("codigostext", query )
                if (x.numero.contains(query)) {
                    Log.e("x", items.toString())
                    items.add(x)
                }
            }
        } else {
            items.addAll(codigos!!)
        }
        Log.e("x", items.toString())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderHistoricoResu {
        return ViewHolderHistoricoResu(
            LayoutInflater.from(context).inflate(
                R.layout.item_resu_historico,
                p0,
                false
            )
        )
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}


class ViewHolderHistoricoResu(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val rota = itemView.rotaadm
    val numero = itemView.codigo
    val dataHist = itemView.datahist
    val opc = itemView.opc


    init {
        itemView.setOnClickListener {
            Values.setPassTipo("rota")
            Values.setPassId(numero.text.toString())
            Values.setPassRota(rota.text.toString())
            val intent = Intent(itemView.getContext(), HistoricoRastreio::class.java)
            itemView.getContext().startActivity(intent)
        }
    }
}



