package br.com.azapfy.azapfyvolumetria.bipador.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.GestorComfirmar
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_rotaadm.view.*

class AdapterListRotaConcluidas(
    val rotas: MutableList<Rota>,
    val context: Context?
) : RecyclerView.Adapter<ViewHoldeRotaConcluida>() {

    override fun onBindViewHolder(p0: ViewHoldeRotaConcluida, p1: Int) {

        p0.rota?.text = rotas.get(p1).rota.toUpperCase()
        p0.data?.text = rotas.get(p1).data.replace("-", "/")
        p0.quantidade?.text = rotas.get(p1).quantidade.toString()
        p0.clientevalue?.text = rotas.get(p1).embarcador
        if (rotas.get(p1)._id == null) {
            p0._id = "0"
        } else {
            p0._id = rotas.get(p1)._id
        }

        p0.porce.visibility = View.GONE
        p0.rotatext.visibility = View.GONE
        p0.porce?.progress = rotas.get(p1).porcentagem
        p0.rotatext?.text = rotas.get(p1).quantidade.toString() + "/" + rotas.get(p1).limite


    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldeRotaConcluida {
        return ViewHoldeRotaConcluida(
            LayoutInflater.from(context).inflate(
                R.layout.item_rotaadm,
                p0,
                false
            )
        )
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return rotas.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}

class ViewHoldeRotaConcluida(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {

    val rota = itemView.rotaadm
    val data = itemView.dataadmrota
    val quantidade = itemView.quantidadeadmrota
    val porce = itemView.progrota
    val rotatext = itemView.rotatext
    val clientevalue = itemView.clientevalue

    var _id = ""

    init {
        itemView.setOnClickListener {
            Log.e("rota", _id)
            Values.setPassTipo("rota")
            Values.setPassId(_id)
            val intent = Intent(itemView.getContext(), GestorComfirmar::class.java)
            itemView.getContext().startActivity(intent)
        }


    }

    override fun onLongClick(v: View?): Boolean {

        return false
    }

    override fun onClick(view: View) {

    }

    companion object {
        private val numero = ArrayList<Int>()
    }


}