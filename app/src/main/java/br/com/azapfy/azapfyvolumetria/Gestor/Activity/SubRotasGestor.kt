package br.com.azapfy.azapfyvolumetria.Gestor.Activity


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListSubRota
import br.com.azapfy.azapfyvolumetria.MainActivity
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_list_completed.*
import java.util.*


class SubRotasGestor : AppCompatActivity() {
    // Initializing an empty ArrayList to be filled with dados
    lateinit var pass: User

    init {
        pass = User()
    }

    var teste = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_subrota)
        lateinit var mAdapter: AdapterListSubRota
        rv_listconcluid.layoutManager = LinearLayoutManager(this)
        Thread {
            val list = AppDatabase.use(this).subRotaDao().findrotas(Values.getPassRota())

            runOnUiThread {
                mAdapter = AdapterListSubRota(
                    list,
                    this
                )
                rv_listconcluid.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListSubRota(ArrayList(), this)
        Values.setPassTipo("")

        this.voltar.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }
// Adds dados to the empty dados ArrayList
}
