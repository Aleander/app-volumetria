package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Romaneio
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.GestorComfirmar
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_romaneioadm.view.*


class AdapterListRomaneioAdm(
    val items: MutableList<Romaneio>,
    val context: Context?
) : RecyclerView.Adapter<ViewHoldeRomaneioAdm>() {
    var nRomaneio = ArrayList<Romaneio>(items)
    override fun onBindViewHolder(p0: ViewHoldeRomaneioAdm, p1: Int) {
        p0.data?.text = nRomaneio.get(p1).data.replace("-" , "/" )
        p0.quanti?.text = nRomaneio.get(p1).quantidade.toString()
        p0.title?.text = nRomaneio.get(p1).nRomaneio
        if( nRomaneio.get(p1)._id == null){
            p0._id = "0"
        }else {
            p0._id = nRomaneio.get(p1)._id
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldeRomaneioAdm {
        return ViewHoldeRomaneioAdm(
            LayoutInflater.from(context).inflate(
                R.layout.item_romaneioadm,
                p0,
                false
            )
        )
    }


    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return nRomaneio.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view

    fun filter(query: String) {
        nRomaneio.clear()
        if (!query.isEmpty()) {
            for (romaneio in items!!) {
                if ((romaneio).embarcador!!.toUpperCase().contains(query.toUpperCase())
                    || (romaneio).tomador!!.toUpperCase().contains(query.toUpperCase())
                    || (romaneio).nRomaneio!!.toUpperCase().contains(query.toUpperCase())
                ) {
                    Log.e("x", nRomaneio.toString())
                    nRomaneio.add(romaneio)
                }
            }
        } else {
            nRomaneio.addAll(items!!)
        }
        Log.e("x", nRomaneio.toString())
        notifyDataSetChanged()
    }
}


class ViewHoldeRomaneioAdm(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {

    override fun onLongClick(v: View?): Boolean {
        Log.e("teste", "1")
        return false
    }

    val title = itemView.title
    val data = itemView.dataadmromaneio
    val textsize = itemView.textsize
    val quanti = itemView.quantidadeadmromaneio
    val romaneio = itemView.romaneioadm
    var _id = ""
    init {
        itemView.setOnClickListener {
            Values.setPassId(_id)
            Values.setPassTipo("romaneio")
            val intent = Intent(itemView.getContext(), GestorComfirmar::class.java)
            itemView.getContext().startActivity(intent)
        }
    }

    override fun onClick(view: View) {

    }

    companion object {
        private val numero = ArrayList<Int>()
    }
}