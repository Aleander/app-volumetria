package br.com.azapfy.azapfyvolumetria.Gestor.fragment
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Romaneio
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListRomaneioAdm
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_fragment_romaneioadm.*

class FragmentRomaneioAdm : Fragment() {

    lateinit var mAdapter: AdapterListRomaneioAdm

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_fragment_romaneioadm, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        romaniolist.layoutManager = LinearLayoutManager(context)
        Thread {
            val all = AppDatabase.use(context).romaneioDao().findallromaneio()
            activity?.runOnUiThread {
                mAdapter = AdapterListRomaneioAdm(
                    all!! as ArrayList<Romaneio>
                    , context
                )
                romaniolist.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListRomaneioAdm(ArrayList(), requireContext())
        atuRomaneioAdm.setOnRefreshListener {
            Log.e("atualizar", "atualiza")
            Thread {
                VolleyGet.AtualizarRotas(requireActivity(), requireContext())
                val all = AppDatabase.use(context).romaneioDao().findallromaneio()
                activity?.runOnUiThread {
                    mAdapter = AdapterListRomaneioAdm(
                        all!! as ArrayList<Romaneio>
                        , context
                    )
                    romaniolist.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()
                    atuRomaneioAdm.isRefreshing = false
                }
            }.start()
        }
        romaneiotext.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mAdapter!!.filter(s.toString())
            }
        })
    }
    fun getAdapter(): AdapterListRomaneioAdm {
        return mAdapter
    }
}