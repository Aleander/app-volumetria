package br.com.azapfy.azapfyvolumetria.Gestor.fragment


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.Gestor.Adapter.AdapterListHistoricoResu
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.activity_fragment_historico.*


class FragmentHistorico : Fragment() {
    private var viewDestroyed = false
    private var refreshAvailable = true
    private lateinit var mView: View
    lateinit var mAdapter: AdapterListHistoricoResu
    lateinit var pass: User
    var aux = 0

    init {
        pass = User()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_fragment_historico, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        listhistorico.layoutManager = LinearLayoutManager(context)
        Thread {
            var codigos = AppDatabase.use(context).codigoDao().finderroRotaErro(true, true)
            activity?.runOnUiThread {
                mAdapter = AdapterListHistoricoResu(
                    codigos!! as ArrayList<Codigo>,
                    requireContext()
                )
                listhistorico.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListHistoricoResu(
            ArrayList(),
            requireContext()

        )
        configureTabLayout()

        atuHistorico.setOnRefreshListener {
            Log.e("atualizar", "atualiza")
            Thread {
                if (aux == 0) {
                    VolleyGet.AtualizarCodigos(requireActivity(), requireContext(), requireActivity())
                    var codigos = AppDatabase.use(context).codigoDao().finderroRotaErro(true, true)
                    activity!!.runOnUiThread {
                        mAdapter = AdapterListHistoricoResu(
                            codigos!! as ArrayList<Codigo>,
                            requireContext()
                        )
                        listhistorico.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()
                        atuHistorico.isRefreshing = false
                    }
                } else if (aux == 1) {
                    var codigos = AppDatabase.use(context).codigoDao().finderroNoVeri(true, true)
                    activity!!.runOnUiThread {
                        mAdapter = AdapterListHistoricoResu(
                            codigos!! as ArrayList<Codigo>,
                            requireContext()
                        )
                        listhistorico.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()
                        atuHistorico.isRefreshing = false
                    }
                } else if (aux == 2) {
                    var codigos = AppDatabase.use(context).codigoDao().finderroNoBip(true, true)
                    activity!!.runOnUiThread {
                        mAdapter = AdapterListHistoricoResu(
                            codigos!! as ArrayList<Codigo>,
                            requireContext()
                        )
                        listhistorico.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()
                        atuHistorico.isRefreshing = false
                    }

                }

            }.start()
        }
    }

    private fun configureTabLayout() {
        filtrosList.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    aux = 0
                    Thread {

                        var codigos = AppDatabase.use(context).codigoDao().finderroRotaErro(true, true)
                        activity!!.runOnUiThread {
                            mAdapter = AdapterListHistoricoResu(
                                codigos!! as ArrayList<Codigo>,
                                requireContext()
                            )
                            listhistorico.adapter = mAdapter
                            mAdapter.notifyDataSetChanged()
                            atuHistorico.isRefreshing = false
                        }
                    }.start()

                } else if (tab.position == 1) {

                    aux = 1
                    Thread {
                        var codigos = AppDatabase.use(context).codigoDao().finderroNoVeri(true, true)
                        activity!!.runOnUiThread {
                            mAdapter = AdapterListHistoricoResu(
                                codigos!! as ArrayList<Codigo>,
                                requireContext()
                            )
                            listhistorico.adapter = mAdapter
                            mAdapter.notifyDataSetChanged()
                            atuHistorico.isRefreshing = false
                        }
                    }.start()
                } else if (tab.position == 2) {
                    aux = 2
                    Thread {
                        var codigos = AppDatabase.use(context).codigoDao().finderroNoBip(true, true)
                        activity!!.runOnUiThread {
                            mAdapter = AdapterListHistoricoResu(
                                codigos!! as ArrayList<Codigo>,
                                requireContext()
                            )
                            listhistorico.adapter = mAdapter
                            mAdapter.notifyDataSetChanged()
                            atuHistorico.isRefreshing = false
                        }
                    }.start()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }

    fun getAdapter(): AdapterListHistoricoResu {
        return mAdapter
    }
}