package br.com.azapfy.azapfyvolumetria.Gestor.Adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.R
import kotlinx.android.synthetic.main.item_historico_erros.view.*


class AdapterListHistorico(val codigos: MutableList<Codigo>, val context: Context) :
    RecyclerView.Adapter<ViewHolderHistorico>() {
    var items = ArrayList<Codigo>(codigos)
    override fun onBindViewHolder(p0: ViewHolderHistorico, p1: Int) {
        var validacao = 0

        if (items.get(p1).romaneio_bip != null || items.get(p1).romaneio_bip != ""
            || items.get(p1).romaneio_veri != null || items.get(p1).romaneio_veri != ""
        ) {


            if (items.get(p1).romaneio_bip != null && items.get(p1).romaneio_bip != "") {
                p0.codigo?.text = items.get(p1).numero
                p0.textrotabip?.text = "Romaneio"
                p0.rota?.text = items.get(p1).romaneio_bip
                if (items.get(p1).descricao != "null") {
                    p0.descricao?.text = items.get(p1).descricao
                } else {
                    p0.descricao?.text = ""
                }
                p0.cpf?.text = items.get(p1).cpfbipador
                p0.data?.text = items.get(p1).databipar
            } else {
                p0.ct_bip?.visibility = View.GONE
                p0.ct_erro?.visibility = View.VISIBLE
                validacao = 1
            }
            if (items.get(p1).romaneio_veri != null && items.get(p1).romaneio_veri != "") {
                p0.codigo?.text = items.get(p1).numero
                p0.textrotaveri?.text = "Romaneio"
                p0.rota_veri?.text = items.get(p1).romaneio_veri
                if (items.get(p1).descricaoveri != "null") {
                    p0.descricao_veri?.text = items.get(p1).descricaoveri
                } else {
                    p0.descricao_veri?.text = ""
                }
                p0.cpf_veri?.text = items.get(p1).cpfveri
                p0.data_veri?.text = items.get(p1).dataveri
            } else {
                p0.ct_veri?.visibility = View.GONE
                p0.ct_erro_veri?.visibility = View.VISIBLE
                validacao = 2
            }
        } else {


            //ROTA
            if (items.get(p1).rota_bip != null && items.get(p1).rota_bip != "") {
                p0.codigo?.text = items.get(p1).numero
                p0.rota?.text = items.get(p1).rota_bip
                if (items.get(p1).descricao != "null") {
                    p0.descricao?.text = items.get(p1).descricao
                } else {
                    p0.descricao?.text = ""
                }
                p0.cpf?.text = items.get(p1).cpfbipador
                p0.data?.text = items.get(p1).databipar
            } else {
                p0.ct_bip?.visibility = View.GONE
                p0.ct_erro?.visibility = View.VISIBLE
                validacao = 1
            }
            if (items.get(p1).rota_veri != null && items.get(p1).rota_veri != "") {
                p0.codigo?.text = items.get(p1).numero
                p0.rota_veri?.text = items.get(p1).rota_veri
                if (items.get(p1).descricaoveri != "null") {
                    p0.descricao_veri?.text = items.get(p1).descricaoveri
                } else {
                    p0.descricao_veri?.text = ""
                }
                p0.cpf_veri?.text = items.get(p1).cpfveri
                p0.data_veri?.text = items.get(p1).dataveri
            } else {
                p0.ct_veri?.visibility = View.GONE
                p0.ct_erro_veri?.visibility = View.VISIBLE
                validacao = 2
            }
        }
        p0.opc.setOnClickListener {
            val v = p0.opc
            val location = IntArray(2)
            v.getLocationOnScreen(location)
            //creating a popup menu
            val popup = PopupMenu(context, v)
            //inflating menu from xml resource
            //adding click listener
            popup.inflate(br.com.azapfy.azapfyvolumetria.R.menu.menu_historico)
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    when (item.itemId) {
                        br.com.azapfy.azapfyvolumetria.R.id.wpp -> {
                            val openURL = Intent(android.content.Intent.ACTION_VIEW)
                            openURL.data = Uri.parse(
                                "https://api.whatsapp.com/send?phone=553187321618text&text=O volume: ${items.get(p1).numero}" +
                                        "%20pertence a rota CURVELO.%20 Gentileza envia-lo para a matriz. %20"
                            )
                            context.startActivity(openURL)
                        }
                        br.com.azapfy.azapfyvolumetria.R.id.ligar -> {
                            val uri = Uri.parse("tel:3187321618")
                            val intent = Intent(Intent.ACTION_DIAL, uri)
                            context.startActivity(intent)
                        }

                        br.com.azapfy.azapfyvolumetria.R.id.aprovar -> {
                            if (validacao == 1) {
                                Thread {
                                    AppDatabase.use(context).codigoDao()
                                        .updatecodigosAprovadoVeri(true, items.get(p1).numero)
                                }.start()
                                VolleyGet.aprovarCodigos(Activity(), context, Activity(), items.get(p1).numero)
                                p0.all?.visibility = View.GONE
                                items.removeAt(p0.adapterPosition);
                                notifyItemRemoved(p0.adapterPosition);
                                notifyItemRangeChanged(p0.adapterPosition, items.size);

                            } else {
                                Thread {
                                    AppDatabase.use(context).codigoDao()
                                        .updatecodigosAprovadoBipa(true, items.get(p1).numero)
                                }.start()
                                VolleyGet.aprovarCodigos(Activity(), context, Activity(), items.get(p1).numero)
                                p0.all?.visibility = View.GONE
                                items.removeAt(p0.adapterPosition);
                                notifyItemRemoved(p0.adapterPosition);
                                notifyItemRangeChanged(p0.adapterPosition, items.size);

                            }
                        }

                    }//handle menu1 click
                    //handle menu2 click
                    //handle menu3 click
                    return false
                }
            })
            //displaying the popup
            popup.show()
        }
    }

    fun filter(query: String) {
        items.clear()
        if (!query.isEmpty()) {
            for (x in codigos!!) {
                Log.e("codigostext", query )
                if (x.numero.contains(query)   ) {
                    Log.e("x", items.toString())
                    items.add(x)
                }
            }
        } else {
            items.addAll(codigos!!)
        }
        Log.e("x", items.toString())
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderHistorico {
        return ViewHolderHistorico(
            LayoutInflater.from(context).inflate(
                R.layout.item_historico_erros,
                p0,
                false
            )
        )
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }
    // Inflates the item views
    // Binds each animal in the ArrayList to a view
}


class ViewHolderHistorico(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val all = itemView.all
    val codigo = itemView.val_codigo
    val rota = itemView.rota
    val textrotabip = itemView.textrotabip
    val textrotaveri = itemView.textrotaveri
    val rota_veri = itemView.rota_veri
    val descricao = itemView.descricao
    val descricao_veri = itemView.descricao_veri
    val cpf = itemView.cpf
    val cpf_veri = itemView.cpf_veri
    val data = itemView.data
    val data_veri = itemView.data_veri
    val ct_bip = itemView.conteudo_bip
    val ct_veri = itemView.conteudo_verif
    val ct_erro = itemView.bip_pendente
    val ct_erro_veri = itemView.verif_pendente
    val opc = itemView.opc
}

