package br.com.azapfy.azapfyvolumetria.verificador.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Romaneio
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Adapter.AdapterListRomaneio
import kotlinx.android.synthetic.main.activity_fragment_romaneio_pendente.*
import java.util.*

class FragmentRomaneioPendente : Fragment() {
    lateinit var mAdapter: AdapterListRomaneio
    private lateinit var mView: View
    private var method = "PENDENTE"
    var nRomaneio = ArrayList<Romaneio>()
    private var filterHasChanged = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.e("create", "ds")
        return inflater.inflate(R.layout.activity_fragment_romaneio_pendente, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        romaneiolistcontadorpendente.layoutManager = LinearLayoutManager(context)
        Thread {
            if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
                nRomaneio = AppDatabase.use(context).romaneioDao().getRomaneioSituacao("bipado") as ArrayList<Romaneio>
            } else {
                nRomaneio =
                    AppDatabase.use(context).romaneioDao().getRomaneioSituacao("cadastrado") as ArrayList<Romaneio>
            }
            activity?.runOnUiThread {
                mAdapter = AdapterListRomaneio(
                        nRomaneio,
                        requireContext()
                    )
                romaneiolistcontadorpendente.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterListRomaneio(
            ArrayList(),
            requireContext()
        )


        romaneiotext.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mAdapter!!.filter(s.toString())
            }
        })
        atuRomaneioPendente.setOnRefreshListener {
            Log.e("atualizar", "atualiza")
            Thread {
                VolleyGet.AtualizarRotas(requireActivity(), requireContext())
                if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
                    nRomaneio =
                        AppDatabase.use(context).romaneioDao().getRomaneioSituacao("bipado") as ArrayList<Romaneio>
                } else {
                    nRomaneio =
                        AppDatabase.use(context).romaneioDao().getRomaneioSituacao("cadastrado") as ArrayList<Romaneio>
                }
                activity!!.runOnUiThread {
                    mAdapter = AdapterListRomaneio(
                        nRomaneio as ArrayList<Romaneio>
                        , requireContext()
                    )
                    romaneiolistcontadorpendente.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()
                    atuRomaneioPendente.isRefreshing = false
                }
            }.start()
        }
    }

    fun getAdapter(): AdapterListRomaneio {
        return mAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}





