package br.com.azapfy.azapfyvolumetria.bipador.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.MainActivity
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Activity.Camera
import br.com.azapfy.azapfyvolumetria.bipador.Adapter.AdapterList
import kotlinx.android.synthetic.main.activity_fragment_list_new.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class FragmentListNew : AppCompatActivity() {
    // Initializing an empty ArrayList to be filled with dados
    lateinit var mAdapter: AdapterList
    var teste = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_list_new)

        rv_animal_list.layoutManager = LinearLayoutManager(this)

        Thread {
            val list = if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
                AppDatabase.use(this).codigoDao().findcodigosPende(true)
            } else {
                AppDatabase.use(this).codigoDao().findcodigos(Values.getPassId())
            }
            runOnUiThread {
                mAdapter = AdapterList(
                    list as ArrayList<Codigo>,
                    this
                )
                rv_animal_list.adapter = mAdapter
            }
        }.start()
        mAdapter = AdapterList(ArrayList(), this)
        this.continuar!!.setOnClickListener {
            val intent = Intent(this, Camera::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent)
        }
        this.voltar!!.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent)
        }
        this.sicro!!.setOnClickListener {
            if (Values.getUser() == "verificador" || Values.getUser() == "admverificador") {
                enviarveri()
            }
            if (Values.getUser() == "bipador" || Values.getUser() == "admbipador") {
                if (Values.getPassTipo() == "romaneio") {
                    enviarbiparomaneio()
                } else if (Values.getPassTipo() == "rota") {
                    enviarbiparrota()
                }
                Toast.makeText(this, "SICRONIZADO COM SUCESSO ", Toast.LENGTH_LONG).show()
                VolleyGet.AtualizarRotas(this, applicationContext)
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
                finish()
            }
        }
    }

    fun enviarveri() {
        val json = JSONObject()
        val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.time = Date()
        val data_atual = cal.time
        try {

            Thread {
                val listaDeCodigos = AppDatabase.use(this).codigoDao().findcodigosPende(true)
                val auxitwo = JSONArray()
                json.put("cnpj_empresa", Values.getCnpj())
                json.put("situacao", "verificada")
                json.put("cpf_veri", Values.getCpf())
                json.put("nome_veri", Values.getNome())
                json.put("telefone_veri", Values.getTelefone())
                json.put("rota_veri", AppDatabase.use(this).volumeDao().findnome(Values.getPassId()))
                json.put("qtn_veri", listaDeCodigos.size)
                for (codigo in listaDeCodigos) {
                    val auxi = JSONObject()
                    auxi.put("codigo", codigo.numero)
                    auxi.put("msg_veri", codigo.descricaoveri)
                    auxi.put("data_veri", codigo.dataveri)
                    auxitwo.put(auxi)
                    AppDatabase.use(this).codigoDao().delete(codigo)
                }
                json.put("codigos", auxitwo)
                Log.e("jsoncodigos", json.toString())
                val connection = Connection(
                    ServicesUrl.getServiceURL(this, ServicesUrl.VALIDARCODIGO), json,
                    this
                )
                HttpPost().postEnviarCodigos(connection)
                AppDatabase.use(connection.ctx).codigoDao().updateCodigosPendentes(false, true)
                VolleyGet.AtualizarCodigos(this, applicationContext, parent)
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
            }.start()
        } catch (f: Exception) {
            LogController.e("sssd", "Error #3: $f")
        }
    }


    fun enviarbiparrota() {
        val json = JSONObject()
        val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.time = Date()
        val data_atual = cal.time
        var data_completa = dateFormat_hora.format(data_atual)
        try {
            Thread {
                val listaDeCodigos = AppDatabase.use(this).codigoDao().findcodigos(Values.getPassId())
                val auxitwo = JSONArray()
                json.put("cnpj_empresa", Values.getCnpj())
                json.put("situacao", "bipado")
                json.put("cpf_bipador", Values.getCpf())
                json.put("nome_bipador", Values.getNome())
                json.put("telefone_bipador", Values.getTelefone())
                json.put("rota_bip", AppDatabase.use(this).volumeDao().findnome(Values.getPassId()))
                json.put("qtn_bipada", listaDeCodigos.size)
                for (codigo in listaDeCodigos) {
                    val auxi = JSONObject()
                    auxi.put("codigo", codigo.numero)
                    auxi.put("msg_bipado", codigo.descricao)
                    auxi.put("data_bip", codigo.databipar)
                    auxitwo.put(auxi)
                }
                json.put("codigos", auxitwo)
                Log.e("jsoncodigos", json.toString())
                val connection = Connection(
                    ServicesUrl.getServiceURL(this, ServicesUrl.SUBROTA), json,
                    this
                )
                var nome = AppDatabase.use(applicationContext).volumeDao().findnome(Values.getPassId())
                AppDatabase.use(this).codigoDao().deleteall(nome, true)
                HttpPost().postEnviarCodigos(connection)
                VolleyGet.AtualizarCodigos(this, applicationContext, parent)
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
            }.start()
        } catch (f: Exception) {
            LogController.e("sssd", "Error #3: $f")
        }
    }

    fun enviarbiparomaneio() {
        val json = JSONObject()
        val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.time = Date()
        val data_atual = cal.time
        var data_completa = dateFormat_hora.format(data_atual)
        try {
            Thread {
                val listaDeCodigos = AppDatabase.use(this).codigoDao().findcodigos(Values.getPassId())
                val auxitwo = JSONArray()
                json.put("cnpj_empresa", Values.getCnpj())
                json.put("situacao", "bipado")
                json.put("cpf_bipador", Values.getCpf())
                json.put("romaneio_bip", Values.getPassromaneio())
                json.put("qtn_bipada", listaDeCodigos.size)
                for (codigo in listaDeCodigos) {
                    val auxi = JSONObject()
                    auxi.put("codigo", codigo.numero)
                    auxi.put("msg_bipado", codigo.descricao)
                    auxi.put("data_bip", codigo.databipar)
                    auxitwo.put(auxi)
                }
                json.put("codigos", auxitwo)
                Log.e("jsoncodigos", json.toString())
                val connection = Connection(
                    ServicesUrl.getServiceURL(this, ServicesUrl.ENVIARCODIGOROMANEIO), json,
                    this
                )
                AppDatabase.use(this).codigoDao().deleteallRomaneio(Values.getPassromaneio(), true)
                HttpPost().postSubRotas(connection)
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
            }.start()
        } catch (f: Exception) {
            LogController.e("sssd", "Error #3: $f")
        }
    }

}