package br.com.azapfy.azapfyvolumetria.verificador.Adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.download.VolleyGet
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Codigo
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.Rota
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.User
import br.com.azapfy.azapfyvolumetria.R
import br.com.azapfy.azapfyvolumetria.bipador.Activity.Camera
import kotlinx.android.synthetic.main.item_rota.view.*


class AdapterListRotaPendente(val items: ArrayList<Rota>, val context: Context?) :
    RecyclerView.Adapter<ViewHoldeRotaContadorPendente>() {

    var rotas = ArrayList<Rota>(items)

    override fun onBindViewHolder(p0: ViewHoldeRotaContadorPendente, p1: Int) {
        p0.rota?.text = rotas.get(p1).rota
        p0.data?.text = rotas.get(p1).data.replace("-", "/")
        p0.id = rotas.get(p1)._id!!
        p0.validacao.visibility = View.GONE
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldeRotaContadorPendente {
        return ViewHoldeRotaContadorPendente(
            LayoutInflater.from(context).inflate(
                R.layout.item_rota,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return rotas.size
    }

    fun filter(query: String) {
        rotas.clear()
        if (!query.isEmpty()) {
            for (rota in items!!) {
                if ((rota).rota!!.toUpperCase().contains(query.toUpperCase())) {
                    Log.e("x", rotas.toString())
                    rotas.add(rota)
                }
            }
        } else {
            rotas.addAll(items!!)
        }
        Log.e("x", rotas.toString())
        notifyDataSetChanged()
    }
}

class ViewHoldeRotaContadorPendente(itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener,
    View.OnLongClickListener {

    override fun onLongClick(v: View?): Boolean {
        Log.e("teste", "1")
        return false
    }

    lateinit var pass: User

    init {
        pass = User()
    }

    val rota = itemView.rota
    val data = itemView.DATAN
    var id = ""
    var validacao = itemView.validacao

    init {

        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
        itemView.setOnClickListener {
            lateinit var pass: User
            lateinit var passC: Codigo

            VolleyGet.AtualizarCodigos(Activity(), itemView.getContext(), Activity())
            Thread {
                pass = User()
                passC = Codigo()
                passC.setPass(1)
                Values.setPassTipo("rota")
                Values.setPassRota(rota.text.toString())
                Values.setPassId(id)
                val intent = Intent(itemView.getContext(), Camera::class.java)
                itemView.getContext().startActivity(intent)
            }.start()
        }
    }

    override fun onClick(view: View) {

    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int, sec: Int)

    }

    companion object {
        private val numero = ArrayList<Int>()
    }
}