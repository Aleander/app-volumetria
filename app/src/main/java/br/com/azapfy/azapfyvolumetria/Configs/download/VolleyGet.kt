package br.com.azapfy.azapfyvolumetria.Configs.download

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.entity.*
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.Gestor.Activity.FilterCod
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


object VolleyGet {
    private lateinit var viewModel: FilterCod
    private var requestQueue: RequestQueue? = null
    fun getRequestQueue(context: Activity): RequestQueue? {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context)
        }
        return requestQueue
    }

    fun request(json: JSONObject, context: Activity?) {
        if (context == null) {
            return
        }
        if (json == null) {
            Log.e("retorno ", "null")
        } else {
            Thread {
                if (json.has("valor")) {
                    val result = json.getJSONArray("valor")
                    for (i in 0 until result.length()) {


                        if (result.getJSONObject(i).has("numero_romaneio")) {
                            val novaRomaneio = Romaneio()
                            novaRomaneio.nRomaneio = result.getJSONObject(i).get("numero_romaneio").toString()
                            if (result.getJSONObject(i).has("_id")) {
                                novaRomaneio._id = result.getJSONObject(i).get("_id").toString()
                            } else {
                                novaRomaneio._id = ""
                            }
                            if (result.getJSONObject(i).has("tomador")) {
                                novaRomaneio.tomador = result.getJSONObject(i).get("tomador").toString()
                            } else {
                                novaRomaneio.tomador = ""
                            }
                            if (result.getJSONObject(i).has("embarcador")) {
                                novaRomaneio.embarcador = result.getJSONObject(i).get("embarcador").toString()
                            } else {
                                novaRomaneio.embarcador = ""
                            }

                            if (result.getJSONObject(i).has("cpf_criador")) {
                                novaRomaneio.cpf = result.getJSONObject(i).get("cpf_criador").toString()
                            } else {
                                novaRomaneio.cpf = ""
                            }
                            if (result.getJSONObject(i).has("cnpj_empresa")) {
                                novaRomaneio.cnpj = result.getJSONObject(i).get("cnpj_empresa").toString()
                            } else {
                                novaRomaneio.cnpj = ""
                            }
                            if (result.getJSONObject(i).has("base")) {
                                novaRomaneio.base = result.getJSONObject(i).get("base").toString()
                            } else {
                                novaRomaneio.base = ""
                            }
                            if (result.getJSONObject(i).has("situacao")) {
                                novaRomaneio.situacao = result.getJSONObject(i).get("situacao").toString()
                            } else {
                                novaRomaneio.situacao = ""
                            }
                            if (result.getJSONObject(i).has("updated_at")) {
                                novaRomaneio.updated_at = result.getJSONObject(i).get("updated_at").toString()
                            } else {
                                novaRomaneio.updated_at = ""
                            }
                            if (result.getJSONObject(i).has("created_at")) {
                                novaRomaneio.data = result.getJSONObject(i).get("created_at").toString()
                            } else {
                                novaRomaneio.data = ""
                            }
                            if (AppDatabase.use(context).romaneioDao().findveriRomaneioId(novaRomaneio._id) != novaRomaneio._id) {
                                AppDatabase.use(context).romaneioDao().insert(novaRomaneio)
                                Log.e("insert", "insert")
                            } else {
                                AppDatabase.use(context).romaneioDao().update(novaRomaneio)
                                Log.e("uodate", "update")
                            }

                        } else if (result.getJSONObject(i).has("rota")) {
                            val novaRota = Rota()
                            if (result.getJSONObject(i).has("_id")) {
                                novaRota._id = result.getJSONObject(i).get("_id").toString()
                            } else {
                                novaRota._id = ""
                            }
                            if (result.getJSONObject(i).has("tomador")) {
                                novaRota.tomador = result.getJSONObject(i).get("tomador").toString()
                            } else {
                                novaRota.tomador = ""
                            }
                            if (result.getJSONObject(i).has("embarcador")) {
                                novaRota.embarcador = result.getJSONObject(i).get("embarcador").toString()
                            } else {
                                novaRota.embarcador = ""
                            }
                            if (result.getJSONObject(i).has("rota")) {
                                novaRota.rota = result.getJSONObject(i).get("rota").toString()
                            } else {
                                novaRota.rota = ""
                            }
                            if (result.getJSONObject(i).has("cpf_criador")) {
                                novaRota.cpf = result.getJSONObject(i).get("cpf_criador").toString()
                            } else {
                                novaRota.cpf = ""
                            }
                            if (result.getJSONObject(i).has("cnpj_empresa")) {
                                novaRota.cnpj = result.getJSONObject(i).get("cnpj_empresa").toString()
                            } else {
                                novaRota.cnpj = ""
                            }
                            if (result.getJSONObject(i).has("base")) {
                                novaRota.base = result.getJSONObject(i).get("base").toString()
                            } else {
                                novaRota.base = ""
                            }
                            if (result.getJSONObject(i).has("situacao")) {
                                novaRota.situacao = result.getJSONObject(i).get("situacao").toString()
                            } else {
                                novaRota.situacao = ""
                            }
                            if (result.getJSONObject(i).has("updated_at")) {
                                novaRota.updated_at = result.getJSONObject(i).get("updated_at").toString()
                            } else {
                                novaRota.updated_at = ""
                            }
                            if (result.getJSONObject(i).has("created_at")) {
                                novaRota.data = result.getJSONObject(i).get("created_at").toString()
                            } else {
                                novaRota.data = ""
                            }
                            if (result.getJSONObject(i).has("quantidade_codigos")) {
                                novaRota.quantidade_bipada_total = result.getJSONObject(i).get("quantidade_codigos").toString()
                            } else {
                                novaRota.quantidade_bipada_total = ""
                            }
                            if (AppDatabase.use(context).volumeDao().findveriRotas(novaRota.rota) != novaRota.rota) {
                                AppDatabase.use(context).volumeDao().insert(novaRota)
                                Log.e("insertrota", "insert")
                            } else {
                                AppDatabase.use(context).volumeDao().updateRota(novaRota.quantidade_bipada_total ,novaRota.rota)
                                Log.e("updaterota", "update" + novaRota.quantidade_bipada_total)
                            }
                            if (result.getJSONObject(i).has("rotas")) {
                                var rotas = result.getJSONObject(i).getJSONObject("rotas")
                                var key = rotas.keys()
                                while (key.hasNext()){
                                    val novaSubRota = SubRota()
                                    val chave = key.next()
                                    novaSubRota.rota = result.getJSONObject(i).get("rota").toString()
                                    novaSubRota.id_rota = chave
                                    if (rotas.getJSONObject(chave).has("data_create")) {
                                        novaSubRota.date_create = rotas.getJSONObject(chave).getString("data_create")
                                    }
                                    if (AppDatabase.use(context).subRotaDao().findveri(chave) != chave) {
                                        AppDatabase.use(context).subRotaDao().insert(novaSubRota)
                                    }
                                }
                            }
                        }
                    }
                }
            }.start()

        }
    }

    fun requestCodigos(json: JSONObject, context: Activity?) {


        if (json == null) {
            Log.e("retorno ", "null")
        } else {
            Thread {
                val result = json.getJSONArray("valor")
                for (i in 0 until result.length()) {
                    val novaCodigo = Codigo()
                    if (result.getJSONObject(i).has("_id")) {
                        novaCodigo._id = result.getJSONObject(i).get("_id").toString()
                    } else {
                        novaCodigo._id = ""
                    }
                    if (result.getJSONObject(i).has("cnpj_empresa")) {
                        novaCodigo.cnpj = result.getJSONObject(i).get("cnpj_empresa").toString()
                    } else {
                        novaCodigo.cnpj = ""
                    }
                    if (result.getJSONObject(i).has("situacao")) {
                        novaCodigo.situacao = result.getJSONObject(i).get("situacao").toString()
                    } else {
                        novaCodigo.situacao = ""
                    }

                    if (result.getJSONObject(i).has("erro")) {
                        novaCodigo.erro = result.getJSONObject(i).getBoolean("erro")
                    } else {
                        novaCodigo.erro = false
                    }

                    if (result.getJSONObject(i).has("aprovado")) {
                        novaCodigo.aprovado = result.getJSONObject(i).getBoolean("aprovado")
                    } else {
                        novaCodigo.aprovado = false
                    }

                    if (result.getJSONObject(i).has("bipado")) {

                        var bipado = result.getJSONObject(i).getJSONObject("bipado")
                        Log.e("tembipado", bipado.toString())
                        if (bipado.has("msg_bipado")) {
                            novaCodigo.descricao = bipado.getString("msg_bipado")
                        } else {
                            novaCodigo.descricao = ""
                        }

                        if (bipado.has("cpf_bipador")) {
                            novaCodigo.cpfbipador = bipado.getString("cpf_bipador")
                        } else {
                            novaCodigo.cpfbipador = ""
                        }

                        if (bipado.has("data_bip")) {
                            novaCodigo.databipar = bipado.getString("data_bip")
                        } else {
                            novaCodigo.databipar = ""
                        }

                        if (bipado.has("subrota_bip")) {
                            novaCodigo.subrota_bip = bipado.getString("subrota_bip")
                        } else {
                            novaCodigo.subrota_bip = ""
                        }

                        if (bipado.has("rota_bip")) {
                            novaCodigo.rota_bip = bipado.getString("rota_bip")
                        } else {
                            novaCodigo.rota_bip = ""
                        }

                        if (bipado.has("romaneio_bip")) {
                            novaCodigo.romaneio_bip = bipado.getString("romaneio_bip")
                        } else {
                            novaCodigo.romaneio_bip = ""
                        }
                        if (bipado.has("telefone_bipador")) {
                            novaCodigo.telefone_bipador = bipado.getString("telefone_bipador")
                        } else {
                            novaCodigo.telefone_bipador = ""
                        }
                        if (bipado.has("nome_bipador")) {
                            novaCodigo.nome_bipador = bipado.getString("nome_bipador")
                        } else {
                            novaCodigo.nome_bipador = ""
                        }

                    }

                    if (result.getJSONObject(i).has("verificador")) {
                        var veri = result.getJSONObject(i).getJSONObject("verificador")
                        Log.e("temveri", veri.toString())
                        if (veri.has("msg_veri")) {
                            novaCodigo.descricaoveri = veri.getString("msg_veri")
                        } else {
                            novaCodigo.descricaoveri = ""
                        }
                        if (veri.has("cpf_veri")) {
                            novaCodigo.cpfveri = veri.getString("cpf_veri")
                        } else {
                            novaCodigo.cpfveri = ""
                        }
                        if (veri.has("data_veri")) {
                            novaCodigo.dataveri = veri.getString("data_veri")
                        } else {
                            novaCodigo.dataveri = ""
                        }
                        if (veri.has("rota_veri")) {
                            novaCodigo.rota_veri = veri.getString("rota_veri")
                        } else {
                            novaCodigo.rota_veri = ""
                        }
                        novaCodigo.verificacao = veri.toString()
                    }
                    var aux = ""
                    if (result.getJSONObject(i).has("reconferido")) {
                        var reconferido = result.getJSONObject(i).getJSONObject("reconferido")
                        Log.e("temreconferido", reconferido.toString())
                        novaCodigo.reconferido = reconferido.toString()
                        if (reconferido.has("rota_reco")) {
                            aux = reconferido.getString("rota_reco")
                        }
                    }

                    if (result.getJSONObject(i).has("codigo") && result.getJSONObject(i).has("verificador") && result.getJSONObject(i).has("bipado")
                    ) {
                        if (novaCodigo.rota_bip.toString() == novaCodigo.rota_veri.toString()) {
                            novaCodigo.numero = result.getJSONObject(i).get("codigo").toString()
                            novaCodigo.erro = false

                        } else if (aux == novaCodigo.rota_bip.toString()) {
                            novaCodigo.numero = result.getJSONObject(i).get("codigo").toString()
                            novaCodigo.erro = false
                        } else {
                            novaCodigo.numero = result.getJSONObject(i).get("codigo").toString()
                            novaCodigo.erro = true
                        }
                    } else if (result.getJSONObject(i).has("codigo") && result.getJSONObject(i).has("verificador")) {
                        novaCodigo.numero = result.getJSONObject(i).get("codigo").toString()
                        novaCodigo.erro = true
                    } else {
                        novaCodigo.numero = result.getJSONObject(i).get("codigo").toString()
                        novaCodigo.erro = true
                    }
                    Log.e("novo codigo", novaCodigo.numero )
                    if (AppDatabase.use(context).codigoDao().findveriNumero(novaCodigo.numero) != novaCodigo.numero
                    ) {
                        AppDatabase.use(context).codigoDao().insert(novaCodigo)
                        Log.e("insert", "insert")
                    } else {
                        Log.e("novoCodigoaddxx", novaCodigo.numero )
                        if (novaCodigo.numero == null || novaCodigo.numero == "") {
                            AppDatabase.use(context).codigoDao().Deletecodi(novaCodigo.numero)
                            AppDatabase.use(context).codigoDao().insert(novaCodigo)
                        }
                        Log.e("update", "update")
                    }
                }
            }.start()
        }
    }

    fun AtualizarRotas(Acti: Activity?, context: Context?) {
        Thread {
            Acti!!.runOnUiThread {
                Log.e("Atualizar ", "Tela")
                val json = JSONObject()
                try {
                    json.put("cnpj_empresa", Values.getCnpj())
                    val connection = Connection(
                        ServicesUrl.getServiceURL(context, ServicesUrl.RETORNAROTAS), json,
                        Acti
                    )
                    HttpPost().post(connection, Acti)
                } catch (e: Exception) {
                    Log.e("Atualizar ", e.toString())
                }

            }
        }.start()
    }

    fun AtualizarCodigos(Acti: Activity?, context: Context?, act: Activity?) {
        Thread {
            Acti!!.runOnUiThread {
                val json = JSONObject()
                try {
                    json.put("cnpj_empresa", Values.getCnpj())
                    val connection = Connection(
                        ServicesUrl.getServiceURL(context, ServicesUrl.RETORNACODIGOS), json,
                        Acti
                    )
                    HttpPost().retornaCodigos(connection, act)
                } catch (e: Exception) {
                    Log.e("Atualizar ", e.toString())
                }

            }
        }.start()
    }

    fun aprovarCodigos(Acti: Activity?, context: Context?, act: Activity?, codigo: String) {
        Thread {
            Acti!!.runOnUiThread {
                val json = JSONObject()
                try {
                    json.put("cnpj_empresa", Values.getCnpj())
                    json.put("codigo", codigo)
                    val connection = Connection(
                        ServicesUrl.getServiceURL(context, ServicesUrl.APROVARCODIGO), json,
                        Acti
                    )
                    Log.e("jsonaprovar", json.toString())
                    HttpPost().retornaCodigos(connection, act)
                } catch (e: Exception) {
                    Log.e("Atualizar ", e.toString())
                }

            }
        }.start()
    }

    fun requestBases(url: String, context: Activity?) {
        if (context == null) {
            return
        }
        val novaBase = Base()
        Thread {
            val stringRequest = StringRequest(url, Response.Listener { response ->
                if (response == "[]") {
                    Toast.makeText(context, "ITEM NAO EXISTE.", Toast.LENGTH_SHORT).show()
                } else {
                    Thread {

                        val bases = JSONObject(response).getJSONArray("valor")
                        Log.e("bases", bases.toString())
                        for (i in 0 until bases.length()) {
                            novaBase.base = bases.getString(i).toString()
                            novaBase.cpnj = Values.getCnpj()
                            if (AppDatabase.use(context).baseDao().findBaseVeri(novaBase.base) == novaBase.base) {
                                Log.e(
                                    "inserido",
                                    " $i base ja incerida cnpj " + novaBase.cpnj + "BASE " + novaBase.base
                                )
                            } else {
                                Log.e("inserido", "cnpj " + novaBase.cpnj + "BASE " + novaBase.base)
                                AppDatabase.use(context).baseDao().insert(novaBase)
                            }
                        }
                    }.start()
                }
            }, Response.ErrorListener {
                Log.e("Teste", "Erro: $it")
            })
            val requestQueue = Volley.newRequestQueue(context)
            requestQueue.add(stringRequest)
        }.start()


    }


    fun AdicionarCodigo(result: JSONObject, context: Activity?, id: String) {
        Thread {
            var key = result.keys()
            while (key.hasNext()) {
                val keyy = key.next()
                val Codigo = Codigo()
                var codigos = result.getJSONObject(keyy)
                Codigo.numero = keyy
                Codigo.cpfbipador = codigos.optString("cpf_bipador").toString()
                Codigo.descricao = codigos.optString("mensagem").toString()
                Codigo.databipar = codigos.optString("dt_bipado").toString()
                if (codigos.has("status")) {
                    Codigo.situacao = codigos.optString("status").toString()
                }

                Log.e("codigobipado", codigos.optString("cpf_bipador").toString() + "idrota" + keyy)
                if (AppDatabase.use(context).codigoDao().findveriNumero(Codigo.numero) != Codigo.numero
                ) {
                    AppDatabase.use(context).codigoDao().insert(Codigo)
                }
            }
        }.start()

    }

    fun requestSubRotas(json: JSONObject, context: Activity?) {
        if (context == null) {
            return
        }
        if (json == null) {
            Log.e("retorno ", "null")
        } else {
            Thread {
                if (json.has("valor")) {
                    val result = json.getJSONObject("valor")
                    var key = result.keys()
                    while (key.hasNext()) {
                        val keyy = key.next()
                        val SubRota = SubRota()
                        SubRota.id_rota = keyy
                        SubRota.rota = json.optString("nome").toString()
                        var subrotajson = result.getJSONObject(keyy)
                        Log.e("subrotas", subrotajson.toString() + "idrota" + keyy)
                        if (AppDatabase.use(context).subRotaDao().findveri(SubRota.id_rota) != SubRota.id_rota
                        ) {
                            Log.e("idrota.numero", SubRota.id_rota)
                            AppDatabase.use(context).subRotaDao().insert(SubRota)
                        }
                    }
                } else {

                }
            }.start()

        }

    }
}

