package br.com.azapfy.azapfyvolumetria.Configs.constants;

public abstract class Constants {
    public static String sede = "sede3.azapfy.com.br";
    public static final String WEBSERVICE_PADRAO = "http://sede3.azapfy.com.br/api/android/";
    public static final String WEBSERVICE_SEDE2 = "http://sede2.azapfy.com.br/api/android/";
    public static final String WEBSERVICE_HOMOLOG = "http://homologacao.azapfy.com.br/api/android/";
    public static String WEBSERVICE_LOCAL = "http://" + "sede3.azapfy.com.br" + "/api/android/";
    public static String PESQUISARCODIGO = "http://" + sede + "/api/volumetria/pesquisacodigo";
    public static String ENVIARROMANEIO = "http://" + sede + "/api/volumetria/cadastraromaneio";
    public static String CONFIRMARGESTOR = "http://" + sede + "/api/volumetria/confirmagestor";
    public static String ENVIARHISTORICO = "http://" + sede + "/api/volumetria/criahistorico";
    public static String ROTACREATE = "http://"+ sede +"/api/volumetria/cadastraRota";
    public static String SUBROTA = "http://"+ sede +"/api/volumetria/cadastraSubRota";
    public static String RETORNASUBROTA = "http://" + sede + "/api/volumetria/listarSubRotas";
    public static String VERIFICARCODIGO = "http://" + sede + "/api/volumetria/validacaoCodigos";
    public static String RETORNACODIGOS = "http://" + sede + "/api/volumetria/retornaCodigos";
    public static String RETORNAROTAS = "http://" + sede + "/api/volumetria/retornaRotas";
    public static String CRIARUSER = "http://" + sede + "/api/users/cadastrar";
    public static String APROVARCODIGO = "http://" + sede + "/api/volumetria/aprovarCodigo";
    public static String ENVIARCODIGOROMA = "http://" + sede + "/api/volumetria/cadastraCodigoRomaneio";
}
