package br.com.azapfy.azapfyvolumetria.bipador.Activity


import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import br.com.azapfy.azapfymotoristas.utils.webservices.Connection
import br.com.azapfy.azapfymotoristas.utils.webservices.LogController
import br.com.azapfy.azapfyvolumetria.Configs.constants.Values
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.controller.DbController
import br.com.azapfy.azapfyvolumetria.Configs.roomdb.database.AppDatabase
import br.com.azapfy.azapfyvolumetria.Configs.upload.HttpPost
import br.com.azapfy.azapfyvolumetria.Configs.url.ServicesUrl
import br.com.azapfy.azapfyvolumetria.R
import com.google.android.gms.vision.barcode.Barcode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_barcode_reader.*
import org.json.JSONObject
import pl.coreorb.selectiondialogs.data.SelectableIcon
import pl.coreorb.selectiondialogs.dialogs.IconSelectDialog
import java.text.SimpleDateFormat
import java.util.*


class BarcodeReaderFragment : Fragment(), IconSelectDialog.OnIconSelectedListener {
    private var mDisposable: Disposable? = null
    private var mIconSelector: IconSelectDialog? = null
    private var mAr: String? = null
    private var mBarcode: Barcode? = null
    private var mStartable = true
    public var listar = arrayListOf<String>()
    var verifi = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Thread {
            var quant = ""
            if (Values.getUser() == "admverificador" || Values.getUser() == "verificador") {
                quant = AppDatabase.use(requireContext()).codigoDao().findquantveriPendVeri(true, Values.getPassRota())
            } else {
                quant = AppDatabase.use(requireContext()).codigoDao().findquantveriPendBipa(true, Values.getPassRota())
            }
            if (quant != null) {
                activity?.runOnUiThread {
                    textquant?.text = "  " + quant!!
                }
            }
        }.start()
        mIconSelector = IconSelectDialog.Builder(context)
            .setIcons(loadOptions())
            .setTitle("Selecione uma ação")
            .setSortIconsByName(true)
            .setOnIconSelectedListener(this)
            .build()
        return inflater.inflate(R.layout.fragment_barcode_reader, container, false)
    }

    override fun onStart() {
        super.onStart()
        mDisposable = barcodeView
            .getObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { barcode ->
                    if (Values.getUser() == "verificador" || Values.getUser() == "admverificador") {
                        onBarcodeRecognizedVerific(barcode.displayValue)
                    } else {
                        onBarcodeRecognized(barcode.displayValue)
                    }
                },
                { throwable ->
                    Log.e("Frag", "${throwable.stackTrace}")
                })
    }
    override fun onStop() {
        super.onStop()
        mDisposable?.dispose()
    }
    private fun onBarcodeRecognized(ar: String) {
        val data = Date()
        val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.time = data
        val data_atual = cal.time
        val data_completa = dateFormat_hora.format(data_atual)
        mAr = ar

        Thread {
            if (AppDatabase.use(requireContext()).codigoDao().findveriNumero(ar) != ar) {
                if (mAr != null) {
                    Values.setDatabip(data_completa)
                    if (Values.getDescricao() != null) {
                        DbController.insertCodigo(
                            requireContext(),
                            ar,
                            Values.getPassRota(),
                            Values.getDescricao(),
                            Values.getPassTipo(),
                            Values.getPassId(),
                            data_completa
                            , Values.getCpf()
                            , Values.getCnpj()
                            , true
                            , "bipado"
                        )
                        Values.setDescricao(null)
                    } else {
                        DbController.insertCodigo(
                            requireContext(),
                            ar,
                            Values.getPassRota(),
                            "",
                            Values.getPassTipo(),
                            Values.getPassId(),
                            data_completa,
                            Values.getCpf()!!,
                            Values.getCnpj()
                            , true
                            , "bipado"
                        )
                    }
                    var quant = AppDatabase.use(requireContext()).codigoDao().findquant(Values.getPassId())
                    Log.e("quantidade bipada", quant.toString() + "   " + Values.getPassId())
                    activity!!.runOnUiThread {
                        textquant.text = "  " + quant
                        Values.setQuantidadebipadas(quant.toString().toInt())
                    }
                }
            } else {
                var codigos = AppDatabase.use(requireContext()).codigoDao().findcodigosPendeBipador(true, ar)
                if (codigos.isNotEmpty()) {
                    if (codigos.get(0).numero == ar) {
                        activity!!.runOnUiThread {
                            Toast.makeText(context, "ESTE ITEM JA FOI BIPADO.", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        insertpop(ar, data_completa)
                    }
                } else {
                    insertpop(ar, data_completa)
                }
            }
        }.start()
    }


    private fun onBarcodeRecognizedRomaneio(ar: String) {
        val data = Date()
        val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.time = data
        val data_atual = cal.time
        val data_completa = dateFormat_hora.format(data_atual)
        mAr = ar
        Thread {
            if (AppDatabase.use(requireContext()).codigoDao().findveriNumero(ar) != ar) {
                if (mAr != null) {
                    Values.setDatabip(data_completa)
                    if (Values.getDescricao() != null) {
                        DbController.insertCodigo(
                            requireContext(),
                            ar,
                            Values.getPassromaneio(),
                            Values.getDescricao(),
                            Values.getPassTipo(),
                            Values.getPassId(),
                            data_completa
                            , Values.getCpf()
                            , Values.getCnpj()
                            , true
                            , "bipado"
                        )
                        Values.setDescricao(null)
                    } else {
                        DbController.insertCodigo(
                            requireContext(),
                            ar,
                            Values.getPassromaneio(),
                            "",
                            Values.getPassTipo(),
                            Values.getPassId(),
                            data_completa,
                            Values.getCpf()!!,
                            Values.getCnpj()
                            , true
                            , "bipado"
                        )
                    }
                    var quant = AppDatabase.use(requireContext()).codigoDao().findquant(Values.getPassId())
                    Log.e("quantidade bipada", quant.toString() + "   " + Values.getPassId())
                    activity!!.runOnUiThread {
                        textquant.text = "  " + quant
                        Values.setQuantidadebipadas(quant.toString().toInt())
                    }
                }
            } else {
                var codigos = AppDatabase.use(requireContext()).codigoDao().findcodigosPendeBipador(true, ar)
                if (codigos.isNotEmpty()) {
                    if (codigos.get(0).numero == ar) {
                        activity!!.runOnUiThread {
                            Toast.makeText(context, "ESTE ITEM JA FOI BIPADO.", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        insertpop(ar, data_completa)
                    }
                } else {
                    insertpop(ar, data_completa)
                }
            }
        }.start()
    }


    fun insertpop(ar: String, data_completa: String) {

        var nome = AppDatabase.use(context).codigoDao().findcodigorota(ar)
        Log.e("busca nome", ar + "codigo " + nome)
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Este código já bipado na rota $nome deseja substituir ?")
        builder.setPositiveButton("SIM",
            DialogInterface.OnClickListener { dialog, which ->
                Thread {
                    AppDatabase.use(context).codigoDao().Deletecodi(ar)
                    Log.e("codigodelete", "codigo")
                    if (Values.getDescricao() != null) {
                        DbController.insertCodigo(
                            requireContext(),
                            ar,
                            Values.getPassRota(),
                            Values.getDescricao(),
                            Values.getPassTipo(),
                            Values.getPassId(), data_completa
                            , Values.getCpf()
                            , Values.getCnpj()
                            , true
                            , "bipado"
                        )
                        Values.setDescricao(null)
                    } else {
                        DbController.insertCodigo(
                            requireContext(),
                            ar,
                            Values.getPassRota(),
                            "",
                            Values.getPassTipo(),
                            Values.getPassId(),
                            data_completa,
                            Values.getCpf()!!,
                            Values.getCnpj()
                            , true
                            , "bipado"
                        )
                    }
                    var quant = AppDatabase.use(requireContext()).codigoDao().findquant(Values.getPassId())
                    activity!!.runOnUiThread {
                        textquant.text = "  " + quant
                        Values.setQuantidadebipadas(quant.toString().toInt())
                    }
                }.start()
            }
        )
        builder.setNegativeButton("NÃO",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
        activity!!.runOnUiThread {
            builder.show()
        }
    }


    fun insertpopveri(ar: String, data_completa: String) {

        var nome = AppDatabase.use(context).codigoDao().findcodigorotaVeri(ar)
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Este código já verificado na rota $nome deseja substituir ?")
        builder.setPositiveButton("SIM",
            DialogInterface.OnClickListener { dialog, which ->
                Thread {
                    Values.setDatabip(data_completa)
                    AppDatabase.use(context).codigoDao().updatecodigos(
                        ar,
                        Values.getCpf(),
                        data_completa,
                        nome,
                        Values.getDescricao(),
                        true
                        , Values.getPassId()
                    )

                    var quant = AppDatabase.use(requireContext()).codigoDao().findquant(Values.getPassId())
                    activity!!.runOnUiThread {
                        textquant.text = "  " + quant
                        Values.setQuantidadebipadas(quant.toString().toInt())
                    }
                }.start()
            }
        )
        builder.setNegativeButton("NÃO",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
        activity!!.runOnUiThread {
            builder.show()
        }
    }

    fun som() {
        var mp = MediaPlayer()
        mp = MediaPlayer.create(requireContext(), R.raw.somerro)
        mp.start()
        val vibrator = getActivity()!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (vibrator.hasVibrator()) { // Vibrator availability checking
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(
                    VibrationEffect.createOneShot(
                        3000,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    )
                )
            } else {
                vibrator.vibrate(
                    3000
                )
            }
        }
    }

    fun enviarhistorico(ar: String) {
        Thread {

            var nome = AppDatabase.use(context).volumeDao().findnome(Values.getPassId())

            val json = JSONObject()
            val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
            val cal = Calendar.getInstance()
            cal.time = Date()
            val data_atual = cal.time
            var data_completa = dateFormat_hora.format(data_atual)
            Log.e("data", data_completa.toString())
            try {
                json.put("cnpj_empresa", Values.getCnpj())
                json.put("situacao", "verificada")
                json.put("cpf_veri", Values.getCpf())
                json.put("rota_veri", AppDatabase.use(requireContext()).volumeDao().findnome(Values.getPassId()))
                json.put("codigo", ar)
                if (Values.getDescricao() != null) {
                    json.put("msg_veri", Values.getDescricao())
                } else {
                    json.put("msg_veri", "")
                }
                json.put("data_veri", data_completa)
                Log.e("historicodeerro", json.toString())
                val connection = Connection(
                    ServicesUrl.getServiceURL(requireContext(), ServicesUrl.ENVIARHISTORICO), json,
                    requireActivity()
                )
                HttpPost().postHist(connection)
                if (Values.getDescricao() != null) {
                    DbController.insertCodigoVeri(
                        requireContext(),
                        ar,
                        nome,
                        Values.getDescricao(),
                        Values.getPassTipo(),
                        Values.getPassId(),
                        data_completa,
                        Values.getCpf(),
                        Values.getCnpj(),
                        true,
                        "verificado"
                    )
                    Values.setDescricao(null)
                } else {
                    DbController.insertCodigoVeri(
                        requireContext(),
                        ar,
                        nome,
                        "",
                        Values.getPassTipo(),
                        Values.getPassId(),
                        data_completa,
                        Values.getCpf(),
                        Values.getCnpj(),
                        true,
                        "verificado"
                    )
                }
            } catch (f: Exception) {
                LogController.e("sssd", "Error #3: $f")
            }
        }.start()
    }

    private fun onBarcodeRecognizedVerific(ar: String) {
        val dateFormat_hora = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.time = Date()
        val data_atual = cal.time
        val data_completa = dateFormat_hora.format(data_atual)
        Log.e("Bancode veri", "...........")

        Thread {
            var nome = AppDatabase.use(context).volumeDao().findnome(Values.getPassId())
            if (ar != AppDatabase.use(context).codigoDao().findverirotanumero(ar, nome)) {
                //VERIFICA SE ESTA PENDENTE E VERIFICADA ALGUMA VEZ
                if (AppDatabase.use(requireContext()).codigoDao().findcodigosPendeVeri(true, ar) == ar) {
                    activity!!.runOnUiThread {
                        Toast.makeText(context, "ESTE ITEM JA FOI ENVIADO PARA O SISTEMA.", Toast.LENGTH_SHORT).show()
                    }

                } else {
                    activity!!.runOnUiThread {
                        Toast.makeText(context, "ESTE ITEM NÃO ESTA NA ROTA.", Toast.LENGTH_SHORT).show()
                    }
                    som()
                    enviarhistorico(ar)
                }
            } else if (AppDatabase.use(requireContext()).codigoDao().findverificador(ar) == ar
            ) {
                if (AppDatabase.use(requireContext()).codigoDao().findcodigosPendeVeri(true, ar) == ar) {
                    insertpopveri(ar, data_completa)
                } else {
                    activity!!.runOnUiThread {
                        Toast.makeText(context, "ESTE ITEM FOI RECONFERIDO.", Toast.LENGTH_SHORT).show()
                    }
                    enviarhistorico(ar)
                }
            } else {
                if (ar != null) {
                    activity!!.runOnUiThread {
                        Toast.makeText(context, "CODIGO CORRETO .", Toast.LENGTH_SHORT).show()
                    }
                    Values.setDatabip(data_completa)
                    AppDatabase.use(context).codigoDao().updatecodigos(
                        ar,
                        Values.getCpf(),
                        data_completa,
                        nome,
                        Values.getDescricao(),
                        true
                        , Values.getPassId()
                    )
                }
            }
            var quant = AppDatabase.use(requireContext()).codigoDao().findquantveriPendVeri(true, Values.getPassRota())
            activity!!.runOnUiThread {
                textquant.text = "  " + quant
                Values.setQuantidadebipadas(quant.toString().toInt())
            }
        }.start()
    }

    private fun loadOptions(): ArrayList<SelectableIcon> {
        val selectionDialogsColors = ArrayList<SelectableIcon>()
        // selectionDialogsColors.add(SelectableIcon("image_entrega", "Entrega", R.drawable.))
        //   selectionDialogsColors.add(SelectableIcon("image_coleta", "Coleta", R.drawable.ic_collect))
        return selectionDialogsColors
    }

    override fun onIconSelected(selectedItem: SelectableIcon?) {

    }

    override fun onResume() {
        super.onResume()
        mStartable = true
    }
}
